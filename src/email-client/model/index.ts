// setup email data with unicode symbols
export interface EmailOptionsInterface  {
    from: string; // sender address
    to: string; // list of receivers
    subject: string; // Subject line
    text?: string; // plain text body
    html?: string; //Email body
    attachments?: any[];
}