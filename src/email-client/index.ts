import { SendEmail } from "./client";
import { route } from "./routes";

const EmailClient = {
    pkg: { name: 'email-client', version: '1' },
    register: async (server) => {
        server.method('SendEmail',SendEmail);
        server.route(route);
    }
}


export default EmailClient;