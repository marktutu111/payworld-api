
const sendEmail = async (request,h) => {
    try {
        const data=request.payload;
        const response = await request.server.methods.SendEmail(data);
        return h.response(
            {
                success:true,
                message:'pending',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    sendEmail
}