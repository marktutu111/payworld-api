import * as nodemailer from "nodemailer";
import { emailConfig } from "../config";
import { EmailOptionsInterface } from "../model";


// export const mailOptions = {
//     from: '"Fred Foo 👻" <foo@example.com>', // sender address
//     to: 'bar@example.com, baz@example.com', // list of receivers
//     subject: 'Hello ✔', // Subject line
//     text: 'Hello world?', // plain text body
//     html: '<b>Hello world?</b>' // html body
// };


export const SendEmail = (mailOptions: EmailOptionsInterface): Promise<any> => new Promise((resolve,reject) => {

    // create reusable transporter object using the default SMTP transport
    const customTransporter = nodemailer.createTransport(
        {
            host:"smtpout.secureserver.net",
            port:3535,
            secureConnection:false,
            auth: {
                user: "info@mypaygenie.com",
                pass: "Info@admin123456"
            }
        }
    )

    // send mail with defined transport object
    customTransporter.sendMail(mailOptions, (error, info) => {
        try {
            if (error) return reject(error);
            resolve(info)
        } catch (err) {
            reject(err);
        }
    });

})
