import * as joi from "joi";
import Handler from "../handler";

export const route = [
    {
        path: '/send',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    from: joi.string().optional().default('info@mypaygenie.com'),
                    to: joi.string().email().required(),
                    subject: joi.string().required(),
                    html: joi.string().required()
                }
            }
        },
        handler:Handler.sendEmail
    }
]