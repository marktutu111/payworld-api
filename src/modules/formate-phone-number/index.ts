/**
 * 
 * @param {string} num Phonenumber required to formatte 
 * l length of number remaining after substracting 9
 */

const NUMBER_LENGTH: number = 7;
const LOCAL_PHONENUMBER_LENGTH: number = 9;


export const formatePhonenumber = (num: string): string => {
        const l = num.length - LOCAL_PHONENUMBER_LENGTH;
        const number = num.substring(l,parseInt(num));
        return `+233${number}`;
}


// 0207573792
export const getPhoneNetwork = (num: string): string => {
        const l = num.length - LOCAL_PHONENUMBER_LENGTH;
        const number = num.substring(l,parseInt(num));
        const networkCode = number.slice(0,-NUMBER_LENGTH).toString();
        if (networkCode.includes('4')) return 'mtn';
        if (networkCode.includes('0')) return 'vodafone';
        if (networkCode.includes('7')) return 'tigo';
        return 'airtel';
}