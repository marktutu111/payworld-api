import * as moment from 'moment';


const ValidateExpired = {
        pkg: {
                name: 'validate-expiry',
                version: '1'
        },
        register: async (server) => {

                const validate = (date) => new Promise((resolve,reject) => {
                        try {

                                const now = moment();
                                const licenceDate = moment(new Date(date));
                                if (now > licenceDate) {
                                        // date is past
                                        resolve(true);
                                } else {
                                        // date is future
                                        resolve(false);
                                }
                
                        } catch (err) {
                                reject(err);
                        }
                })


                return server.method('ValidateIdExpired', validate);

        }
}


export default ValidateExpired;