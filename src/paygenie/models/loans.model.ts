
// Loan amount_remaining is the total increament of loans 
// taken by a customer with interest
// loan amount_remaining increases to the maximum limit required by a customer.


export class LoanModel {

    rate:string;
    amount_paid:number;
    penalty:number;
    duration:string;
    amount:number;
    amount_remaining:number;
    amountBorrowedWithInterest:number;
    createdAt:Date;
    customer:string | any;
    defaulted:boolean;
    duration_type:string;
    employer:string;
    next_payment_date:Date;
    paidout:boolean;
    status:'pending' | 'failed' | 'paid';
    updatedAt:Date;
    penalty_amount:number;
    maxLoan:number;
    canBorrow:boolean;
    _id:string;
    count:number;
    last_borrow_date:Date;
    can_borrow_amount:number;
    date_loan_paid:Date;

    private borrowing:number=0;
    private loan:any;

    constructor(loan) {
        this.customer = loan.customer;
        this.employer = loan.employer;
        this.defaulted = loan.defaulted;
        this.duration = loan.duration;
        this.next_payment_date = loan.next_payment_date;
        this.status = loan.status;
        this.updatedAt = loan.updatedAt;
        this.createdAt = loan.createdAt;
        this.duration_type = loan.duration_type;
        this.rate = loan.rate;
        this.penalty = loan.penalty;
        this.paidout = loan.paidout;
        this.amount_remaining = loan.amount_remaining;
        this.amount_paid = loan.amount_paid;
        this.amount = loan.amount;
        this.penalty_amount = loan.penalty_amount;
        this.maxLoan = loan.maxLoan;
        this.canBorrow = loan.canBorrow;
        this._id = loan._id;
        this.count = loan.count;
        this.last_borrow_date = loan.last_borrow_date;
        this.can_borrow_amount = loan.can_borrow_amount;
        this.date_loan_paid = loan.date_loan_paid;
        this.amountBorrowedWithInterest=loan.amountBorrowedWithInterest;

        this.loan=loan;
        this.borrowing=this.amount;

    };


    aggregate=(status:'paid' | 'failed',server?:any)=>{
        let loanstatus: 'pending' | 'failed' | 'paid' | null = this.status;
        let paidout: boolean = this.paidout;
        let loans_successful: number = 0;
        let loans_failed: number = 0;
        let current_loan: number = this.amount_remaining;
        let total_loans_taken: number = 0;
        let current_loan_id: any = null;
        let loanscount:number = this.count;

        let can_borrow_again:boolean=true;
        let can_borrow_amount:number=this.can_borrow_amount;
        let last_borrow_date:any=this.last_borrow_date;
        let amount_remaining:number=0;
        let amountBorrowedWithInterest:number=0;
        let borrowing:number=0;

        switch (status) {
            case 'paid':
                loanstatus='pending';
                paidout=true;
                loanscount=1;
                loans_successful=1;
                current_loan=this.getCurrentLoan();
                total_loans_taken=this.amount;
                current_loan_id=this._id;
                can_borrow_again=this.canBorrowExtra();
                can_borrow_amount=this.canBorrowAmount(),
                last_borrow_date=Date.now();
                amount_remaining=this.getInterest();
                amountBorrowedWithInterest=this.getInterest();
                borrowing=this.amount;
                break;
            case 'failed':
                loans_failed = 1;
                if (!this.paidout && this.status !== 'pending') {
                    loanstatus='failed';
                    paidout=false;
                }
                break;
            default:
                break;
        };

        if(this.paidout){
            server.methods.dbLoans.findOneAndUpdate(
                {
                    _id:this._id
                },
                {
                    $inc:{
                        amount_remaining:amount_remaining,
                        amountBorrowedWithInterest:amountBorrowedWithInterest
                    }
                }
            ).catch(err=>server.methods.dbErrors.create(
                {
                    id:'LOAN AGGREGATE UPDATE ERROR',
                    message:err.message,
                    error:err
                }
            ));
        }

        server.methods.dbLoans.findOneAndUpdate(
            {
                _id:this._id
            },
            {
                $set: {
                    paidout:paidout,
                    status:loanstatus,
                    updatedAt:Date.now(),
                    canBorrow:can_borrow_again,
                    can_borrow_amount:can_borrow_amount,
                    last_borrow_date:last_borrow_date
                }
            }
        ).catch(err=>server.methods.dbErrors.create(
            {
                id:'LOAN AGGREGATE UPDATE ERROR',
                message: err.message,
                error: err
            }
        ));
        return {
            status,
            loanstatus,
            paidout,
            loans_successful,
            loans_failed,
            current_loan,
            total_loans_taken,
            current_loan_id,
            loanscount,
            can_borrow_again,
            can_borrow_amount,
            last_borrow_date,
            amount_remaining,
            amountBorrowedWithInterest,
            borrowing
        }
    }


    setBorrowingAmount=(amount:any)=>{
        switch (typeof amount) {
            case 'string':
                this.borrowing=parseFloat(amount);
                break;
            case 'number':
                this.borrowing=amount;
                break;
            default:
                break;
        }
    }

    getName() {
        try {
            const { firstname, lastname } = this.customer;
            return `${firstname} ${lastname}`;
        } catch (err) {
            return '';
        }
    }

    getDuration() {
        try {
            return `${this.duration} ${this.duration_type}`;
        } catch (err) {
            return '';
        }
    }

    getStatus() {
        try {
            return typeof this.status === 'string' ? this.status : 'failed';
        } catch (err) {
            return 'failed';
        }
    }

    getCurrentLoan() {
        switch (this.paidout) {
            case true:
                return this.amount_remaining + this.getInterest();
            default:
                return this.amount_remaining;
        }
    }

    getPaymentDate() {
        switch (this.status) {
            case 'paid':
                return new Date(this.date_loan_paid).toDateString();
            default:
                return new Date(this.next_payment_date).toDateString();
        }
    }

    enablePay() {
        if (this.paidout && this.status === 'pending') {
            return true;
        }
        return false;
    }

    getLoanLastDate() {
        try {
            if (this.last_borrow_date !== null) {
                return new Date(this.last_borrow_date).toDateString();
            }
            return 'Not available';
        } catch (err) {
            return '';
        }
    }

    getDefaulted =():string=> this.defaulted ? 'Yes' : 'No';

    
    canBorrowExtra=():boolean=>{
        if (this.canBorrowAmount() > 0){
            return true;
        }
        return false;
    }

    getBorrowingAmount():number{
        return this.borrowing;
    }


    borrowLimit=(maxnet:string,percentage:string="30"):number=>{
        const MAXNET=parseFloat(maxnet);
        const PERCENTAGE=parseFloat(percentage) / 100;
        return PERCENTAGE*MAXNET;
        
    }

    canBorrowAmount=():number=>{
        let amount =this.can_borrow_amount-this.borrowing;
        return amount <= 0 ? 0 : amount;
    }

    getInterest=(rate?:string):number => {
        let _rate =parseFloat(rate || this.rate);
        let interest = (_rate / 100) * this.borrowing;
        return interest + this.borrowing;
    }

    canBorrowStart=()=>{
        if (this.borrowing > 0 && this.borrowing <= this.can_borrow_amount){
            return true;
        }
        return false;
    }

    owing=()=>{
        return !this.canBorrow || this.defaulted ? true:false;
    }

    getLoan=()=>this.loan;
    

}