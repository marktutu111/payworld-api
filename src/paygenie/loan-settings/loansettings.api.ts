import { settings } from "./database/loansettings.db";
import { route } from "./routes/loansettings.route";

const api = {
    pkg: {
        name: 'loan-settings',
        version: '1'
    },
    register: async (server) => {
        server.method('dbLoanSettings',settings);
        server.route(route);
    }
}

export default api;