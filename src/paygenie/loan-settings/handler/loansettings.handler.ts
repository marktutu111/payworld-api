import * as mongoose from "mongoose";


const getSettings = async (request,h) => {
    try {
        const results = await request.server.methods.dbLoanSettings.find(
            {}
        ).populate('employer');
        return h.response(
            {
                success:true,
                message:'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const getId = async (request,h) => {
    try {
        const {id}=request.params;
        if(!mongoose.Types.ObjectId.isValid(id))throw Error('Invalid ID');
        const results = await request.server.methods.dbLoanSettings.findOne(
            {
                $or: [
                    {
                        _id:id
                    },
                    {
                        employer:id
                    }
                ]
            }
        ).populate('employer');
        if(!results || !results._id){
            throw Error('Loan settings for employer not available');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}

const newSettings = async (request,h) => {
    try {
        const payload=request.payload;
        let data = {};
        Object.keys(payload).forEach(key => {
            if (key && payload[key] !== '') {
                data[key]=payload[key];
            }
        })
        const employer=await request.server.methods.dbEmployer.findOne(
            {
                _id:data['employer']
            }
        )
        if(!employer || !employer._id){
            throw Error('Employer not found in the system');
        }
        const results = await request.server.methods.dbLoanSettings.findOneAndUpdate(
            {
                type:payload['type'],
                employer:data['employer']
            },
            {
                $set: data
            },
            {
                new:true,
                upsert:true
            }
        )
        if (results._id) return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
        throw Error('failed');
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const updateSettings=async(request,h)=>{
    try {
        const {
            id,
            data
        } = request.payload;

        let valid = {};
        Object.keys(data).forEach(key => {
            if (key && data[key] !== '') {
                valid[key]=data[key];
            }
        });
        const results = await request.server.methods.dbLoanSettings.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{...valid,updatedAt:Date.now()}
            }
        );
        if (!results || !results._id){
            throw Error('Update failed');
        }
        return h.response(
            {
                success:true,
                message:'updated',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    getSettings,
    newSettings,
    getId,
    updateSettings
}