import * as mongoose from "mongoose";


const schema = new mongoose.Schema(
    {
        type: {
            type: String,
            enum: [
                'default',
                'employer'
            ],
            default: 'default'
        },
        employer: {
            type: mongoose.Schema.Types.ObjectId,
            default: null,
            ref: 'employer'
        },
        rate: {
            type: Number,
            default: 0
        },
        penalty: {
            type: Number,
            default: 0
        },
        duration: {
            type: String,
            default: '30'
        },
        duration_type: {
            type: String,
            default: 'months',
            enum: [
                'weeks',
                'months',
                'days'
            ]
        },
        borrow_limit:{
            type:Number,
            default:30
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    }
)


export const settings = mongoose.model('loansettings',schema);