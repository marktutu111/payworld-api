import * as joi from "joi";
import Handler from "../handler/loansettings.handler";

export const route = [
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getSettings
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getId
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    data:joi.object().required()
                }
            }
        },
        handler:Handler.updateSettings
    },
    {
        path: '/new',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    type: joi.string().optional().default('default').allow(
                        [
                            '',
                            null
                        ]
                    ).valid(
                        [
                            'default',
                            'employer'
                        ]
                    ),
                    employer: joi.string().optional().allow(
                        [
                            null,
                            ''
                        ]
                    ),
                    rate: joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).optional().allow(
                        [
                            '',
                            null
                        ]
                    ),
                    penalty: joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).optional().allow(
                        [
                            '',
                            null
                        ]
                    ),
                    duration: joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).optional().allow(
                        [
                            '',
                            null
                        ]
                    ),
                    duration_type: joi.string().optional().allow(
                        [
                            'weeks',
                            'months',
                            'days',
                            'hours'
                        ]
                    ),
                    borrow_limit:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).required()
                }
            }
        },
        handler: Handler.newSettings
    }
]