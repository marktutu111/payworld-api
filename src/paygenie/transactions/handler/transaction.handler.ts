

const getAll = async (request,h) => {
    try {
        const results = await request.server.methods.dbTransactions.find(
            {}
        ).limit(1000).sort({createdAt:-1}).populate('loan');
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: true,
                message: err.message
            }
        )
    }
}



const getId = async (request,h) => {
    try {
        const {id}=request.params;
        const results = await request.server.methods.dbTransactions.find(
            {
                $or: [
                    {
                        customer:id
                    },
                    {
                        loan:id
                    },
                    {
                        _id:id
                    }
                ]
            }
        ).populate('loan').populate('customer').sort({createdAt:-1});
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: true,
                message: err.message
            }
        )
    }
}


export default {
    getAll,
    getId
}