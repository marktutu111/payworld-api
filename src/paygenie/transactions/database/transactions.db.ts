import * as mongoose from "mongoose";


const schema = new mongoose.Schema(
    {
        bill:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bills'
        },
        loan: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'loans'
        },
        customer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'customers'
        },
        callbackRef: {
            type: String,
            default:null
        },
        service_type:{
            type:String,
            default:'loan',
            enum:[
                'loan',
                'bill'
            ]
        },
        transaction_type: {
            type: String,
            enum: [
                'debit',
                'credit',
                'refund'
            ]
        },
        status: {
            type: String,
            default: 'pending',
            enum: [
                'pending',
                'failed',
                'paid'
            ]
        },
        account_issuer: {
            type: String,
            required: true
        },
        account_number: {
            type: String,
            required:true
        },
        account_type: {
            type: String,
            enum: [
                'momo',
                'bank'
            ]
        },
        bill_refund:{
            type:Object
        },
        amount: {
            type: Number,
            default:0
        },
        transaction: {
            type: Object,
            default: {}
        },
        updatedAt: {
            type: Date,
            default: Date.now
        },
        createdAt: {
            type: Date,
            default: Date.now
        }
    }
)


export const transactionSchema = mongoose.model('transactions',schema);