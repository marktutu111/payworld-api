import * as joi from "joi";
import Handler from "../handler/transaction.handler";

export const route = [
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getAll
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getId
    }
]