import { transactionSchema } from "./database/transactions.db";
import { route } from "./routes/transaction.route";

const transaction = {
    pkg: {
        name: 'transactions',
        version: '1'
    },
    register: async (server) => {
        server.method('dbTransactions',transactionSchema);
        server.route(route);
    }
}

export default transaction;