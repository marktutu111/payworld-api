import * as joi from "joi";
import Handler from "../handler/invoice.handler";

export const route=[
    {
        path:'/pay',
        method:"POST",
        config:{
            validate:{
                payload:{
                    invoiceId:joi.string().required(),
                    employer:joi.string().required(),
                    amount:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).required(),
                    date:joi.string().required()
                }
            }
        },
        handler:Handler.payInvoice
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getInvoice
    },
    {
        path:'/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getInvoice
    },
    {
        path:'/update',
        method:'PUT',
        handler:Handler.UpdateInvoice
    }
]