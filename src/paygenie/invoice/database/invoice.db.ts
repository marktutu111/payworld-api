import * as mongoose from "mongoose";
import * as randomize from "randomatic";
import { schema } from "../../loans/database/loans.db";


const _schema=new mongoose.Schema(
    {
        invoiceId:{
            type:String,
            required:true
        },
        amount:{
            type:Number,
            default:0
        },
        employer:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'employer'
        },
        datePaid:{
            type:Date,
        },
        status:{
            type:String,
            default:'pending',
            enum:[
                'paid',
                'pending'
            ]
        },
        totalLoans:{
            type:Number,
            default:0
        },
        amountBorrowed:{
            type:Number,
            default:0
        },
        amountBorrowedWithInterest:{
            type:Number,
            default:0
        },
        penalty_accrued:{
            type:Number,
            default:0
        },
        outstanding_amount:{
            type:Number,
            default:0
        },
        amountPaid:{
            type:Number,
            default:0
        },
        loans:{
            type:[schema],
            default:[]
        },
        dateIssued:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const invoice=mongoose.model('invoice',_schema);