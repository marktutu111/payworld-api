import { invoice } from "./database/invoice.db";
import { route } from "./routes/invoice.route";

const api={
    pkg:{
        name:'invoice-api',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbInvoice',invoice);
        server.route(route);
    }
}

export default api;