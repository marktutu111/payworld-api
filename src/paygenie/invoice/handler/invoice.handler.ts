import { formatePhonenumber } from "../../../modules/formate-phone-number";



const payInvoice=async (request,h)=>{
    try {
        const data=request.payload;
        const employer=await request.server.methods.dbEmployer.findOne(
            {
                empcode:data.employer
            }
        )

        if (!employer || !employer._id){
            throw Error('Employer not found');
        }

        const invoice= await request.server.methods.dbInvoice.findOneAndUpdate(
            {
                invoiceId:data.invoiceId,
                status:'pending'
            },
            {
                $set:{
                    status:'paid',
                    amountPaid:data.amount,
                    datePaid:new Date(data.date)
                }
            },{new:true}
        );

        if (!invoice || !invoice._id){
            throw Error('Sorry, We did not find any matching invoice with the details provided');
        }

        employer.update(
            {
                $inc:{
                    total_loan_due:-invoice.amount
                }
            }
        ).catch(err=>null);
        const loans:any[]=invoice.loans;
        loans.forEach(async loan=>{
            try {
                const customer= await request.server.methods.dbCustomers.findOne(
                    {
                        _id:loan.customer
                    }
                );
                const _loan= await request.server.methods.dbLoans.findOne(
                    {
                        _id:loan._id
                    }
                )
                if (!_loan || !_loan._id){
                    throw Error('Loan not found!');
                }
                if (_loan.status==='paid'){

                    const {
                        mobile,
                        account_issuer
                    }=customer;
                    if(!account_issuer){
                        throw Error('Account issuer not found!');
                    }
                    // check the amount difference and put
                    // penalty on employer
                    if(_loan.amount_paid > loan.amount_remaining){
                        const diff=_loan.amount_paid - loan.amount_remaining;
                        employer.update(
                            {
                                $inc:{
                                    penalty_accrued:diff
                                }
                            }
                        ).catch(err=>request.server.methods.dbErrors.create(
                            {
                                id:'invoice-penalty-accrued-error',
                                message:err.message,
                                error:err
                            }
                        ).catch(err=>null))
                    }
                    
                    try {
                        const payload = {
                            kuwaita: 'mikopo',
                            amount:_loan.amount_paid,
                            mno:account_issuer,
                            refID:_loan._id,
                            msisdn:mobile
                        }
                        const response= await request.server.methods.nsanoPay(payload);
                        _loan.update(
                            {
                                cashbackTransaction:response,
                                cashbackAmount:_loan.amount_paid,
                                cashbackReference:response.reference
                            }
                        ).catch(err=>null);
                    } catch (err) {
                        _loan.update(
                            {
                                cashbackTransaction:err,
                                cashbackAmount:_loan.amount_paid
                            }
                        ).catch(err=>null);
                    }
                } else {
                    const amount_remaining=_loan.amount_remaining-loan.amount_remaining;
                    if (Math.round(amount_remaining)<=0) {
                        customer.update(
                            {
                                current_loan_id:null,
                                current_loan:0
                            }
                        ).catch(err=>null);
                        _loan.update(
                            {
                                status:'paid',
                                canBorrow:false,
                                amount_remaining:0
                            }
                        ).catch(err=>null);
                        const message = 'Congratulations ' + customer.firstname + '!'
                        + '\n'
                        + 'Your loan has been paid by your employer, you can request for a new one.'
                        + '\n'
                        + 'Thank you for using PayGenie.';
                        request.server.methods.nsanoSendSMS(
                            {
                                type:'single',
                                data:{
                                    recipient: formatePhonenumber(customer.mobile),
                                    message: message
                                }
                            }
                        ).catch(err=>null);
                    } else {
                        _loan.update(
                            {
                                amount_remaining:amount_remaining
                            }
                        ).catch(err=>null);
                        customer.update(
                            {
                                current_loan:amount_remaining
                            }
                        ).catch(err=>null);
                        const message = 'Congratulations ' + customer.firstname + '!'
                        + '\n'
                        + 'Your employer has paid GHS' + loan.amount_remaining + ' '
                        + 'to settle part of your loan.'
                        + '\n'
                        + 'Thank you for using PayGenie.';
                        request.server.methods.nsanoSendSMS(
                            {
                                type:'single',
                                data:{
                                    recipient: formatePhonenumber(customer.mobile),
                                    message: message
                                }
                            }
                        ).catch(err=>null);
                    }
                }
            } catch (err) {
                request.server.methods.dbErrors.create(
                    {
                        id:'invoice',
                        message:err.message,
                        error:err
                    }
                ).catch(err=>null);
            }
        });
        const html=await request.server.methods.generateInvoice(
            {
                employer:employer['name'],
                date:Date.now(),
                invoiceNumber:invoice['invoiceId'],
                amountBorrowed:invoice['amountBorrowed'],
                totalLoans:invoice['totalLoans'],
                amountBorrowedWithInterest:invoice['amountBorrowedWithInterest'],
                issueDate:invoice['dateIssued'],
                datePaid:new Date().toDateString()
            }, 'RECIEPT'
        );
        const pdf= await request.server.methods.createPDFStream(html);
        request.server.methods.SendEmail(
            {
                from:'info@mypaygenie.com',
                to:employer['email'],
                subject:'PayGenie Invoice',
                html:`
                    <p>Hello ${employer.name}</p>
                    <p>Kindly find attached your reciept for payment made this month.</p>
                    <p>Best Regards.</p>
                `,
                attachments:[
                    {
                        filename: 'invoice.pdf',
                        content:pdf
                    }
                ]
            }
        ).catch(err=>request.server.methods.dbErrors.create(
            {
                id:'INVOICE-RECEIPT-ERROR',
                message:err.message,
                error:err
            }
        ));
        return h.response(
            {
                success:true,
                message:'Invoice has been received for processing.',
                data:invoice
            }
        )
    } catch (err) {
        return h.response(
            {
                success:true,
                message:err.message
            }
        )
    }
}



const getInvoice=async(request,h)=>{
    try {
        const {id}=request.params;
        let filter=typeof id=='string' ? { $or: [{employer:id},{_id:id}] }:{};
        const results=await request.server.methods.dbInvoice.find(
            filter
        ).sort({dateIssued:-1})
            .limit(1000)
            .populate('employer')
            .populate('loans.customer')
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const UpdateInvoice=async(request,h)=>{
    try {

        const {
            id,
            data
        } = request.payload;

        let valid = {};
        Object.keys(data).forEach(key => {
            if (key && data[key] !== '') {
                valid[key]=data[key];
            }
        });
        const invoice=await request.server.methods.dbInvoice.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set:{
                    ...valid,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        if (!invoice || !invoice._id){
            throw Error('Oops!, Invoice not updated')
        }
        return h.response(
            {
                success:true,
                message:'Invoice updated successfully',
                data:invoice
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}




export default {
    payInvoice,
    getInvoice,
    UpdateInvoice
}