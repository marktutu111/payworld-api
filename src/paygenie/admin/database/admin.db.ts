import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        name:String,
        username:String,
        password:String,
        blocked:{
            type:Boolean,
            default:false
        },
        role:{
            type:String,
            default:'superuser',
            enum:[
                'superuser',
                'normal'
            ]
        },
        createdAt:{
            type:Date,
            default:Date.now
        }
    }
)

export const admin=mongoose.model('admin',schema);