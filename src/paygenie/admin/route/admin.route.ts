import Handler from "../handler/admin.hadnler";
import * as joi from "joi";

export const route=[
    {
        path:'/login',
        method:'POST',
        handler:Handler.loginHandle
    },
    {
        path:'/new',
        method:'POST',
        config:{
            validate:{
                payload:{
                    username:joi.string().required(),
                    password:joi.string().required(),
                    name:joi.string().required()
                }
            }
        },
        handler:Handler.newAdmin
    },
    {
        path:'/summary/get',
        method:'GET',
        handler:Handler.summary
    }
]