import * as bcrypt from "bcrypt";


const summary=async(request,h)=>{
    try {
        const results= await Promise.all(
            [
                request.server.methods.dbLoans.aggregate(
                    [
                        {
                            $match: {
                                $or:[
                                    {
                                        status:'pending'
                                    },
                                    {
                                        status:'paid'
                                    }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                amountBorrowedWithInterest: { $sum: "$amountBorrowedWithInterest" },
                                amountBorrowed: { $sum:"$amount" },
                                amountRemaining:{$sum:"$amount_remaining"},
                                totalPayment:{$sum:"$amount_paid"},
                                count: { $sum: 1 }
                            }
                        }
                    ]
                ),
                request.server.methods.dbCustomers.count({}),
                request.server.methods.dbEmployer.count({}),
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:{
                    loans:results[0][0],
                    customers:results[1],
                    employer:results[2]
                }
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const newAdmin=async(request,h)=>{
    try {
        const data=request.payload;
        const admin=await request.server.methods.dbAdmin.findOne(
            {
                username:data.username
            }
        )
        
        if (admin && admin._id) {
            throw Error('Username is already in the system')
        }
        const response=await request.server.methods.dbAdmin.create(data);
        return h.response(
            {
                success:true,
                message:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const loginHandle = async (request, h) => {
    try {

        const { 
            username, 
            password 
        } = request.payload;
        const admin = await request.server.methods.dbAdmin.findOne(
            { 
                username:username,
                password:password,
                blocked: false
            }
        )
        if (admin && admin['_id']) {
            const token: string = request.server.methods.generateJWT(admin.toObject(), '1 year');
            return h.response(
                {
                    success: true,
                    message: 'success',
                    data: admin,
                    token: token
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        );

    }
}



export default {
    loginHandle,
    newAdmin,
    summary
}