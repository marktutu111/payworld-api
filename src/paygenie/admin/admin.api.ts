import { admin } from "./database/admin.db";
import { route } from "./route/admin.route";

const api={
    pkg:{
        name:'admin-api',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbAdmin',admin);
        server.route(route);
    }
}

export default api;