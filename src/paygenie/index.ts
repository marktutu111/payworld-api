import customers from "./customers/customer.api";
import Employer from "./employer/employer.api";
import Loans from "./loans/loans.api";
import Logs from "./errors/errors.api";
import Otp from "./otp";
import Transactions from "./transactions/transactions.api";
import LoanSettings from "./loan-settings/loansettings.api";
import Bills from "./bills/bills.api";
import Invoice from "./invoice/invoice.api";
import Admin from "./admin/admin.api";
import Aggregate from "./aggregate/aggregate.api";



const Paygenie = {
    pkg: {
        name: 'server-routes',
        version: '1'
    },
    register: async (server) => {
        server.register(
            [
                {
                    plugin: Invoice,
                    routes: {
                        prefix: '/paygenie/invoice'
                    }
                },
                {
                    plugin: Admin,
                    routes: {
                        prefix: '/paygenie/admin'
                    }
                },
                {
                    plugin: customers,
                    routes: {
                        prefix: '/paygenie/customers'
                    }
                },
                {
                    plugin: Employer,
                    routes: {
                        prefix: '/paygenie/employer'
                    }
                },
                {
                    plugin: Loans,
                    routes: {
                        prefix: '/paygenie/loans'
                    }
                },
                {
                    plugin: Logs,
                    routes: {
                        prefix: '/paygenie/logs'
                    }
                },
                {
                    plugin: Otp,
                    routes: {
                        prefix: '/paygenie/otp'
                    }
                },
                {
                    plugin: Transactions,
                    routes: {
                        prefix: '/paygenie/transactions'
                    }
                },
                {
                    plugin: LoanSettings,
                    routes: {
                        prefix: '/paygenie/loansettings'
                    }
                },
                {
                    plugin: Bills,
                    routes: {
                        prefix: '/paygenie/bills'
                    }
                },
                {
                    plugin:Aggregate,
                    routes:{
                        prefix:'/paygenie/aggregate'
                    }
                }
            ]
        )
    }
}


export default Paygenie;