import { routes } from "./routes/customer.route";
import { customerDb } from "./db/customers.db";

const customers = {
    pkg: {
        name: 'customers-api',
        version: '1'
    },
    register: async (server) => {
        server.method('dbCustomers',customerDb);
        server.route(routes);
    }
}

export default customers;