import * as mongoose from "mongoose";


const schema = new mongoose.Schema(
    {
        firstname: {
            type: String,
            required: true
        },
        lastname: {
            type: String,
            required: true
        },
        empId: {
            type: String,
            ref: 'employer'
        },
        empcode: {
            type: String,
            required: true
        },
        gender: {
            type: String,
            uppercase: true,
            enum: [
                'MALE',
                'FEMALE'
            ]
        },
        maxnet: {
            type: Number,
            default: 0
        },
        mobile: {
            type: String,
            required:true
        },
        account_issuer:{
            type:String,
            default:null
        },
        email: {
            type: String,
            required:true
        },
        password: {
            type:String,
            maxlength:5,
            default:null
        },
        dob: {
            type:Date,
            required:true
        },
        approved: {
            type: Boolean,
            default: false
        },
        phone_verified: {
            type: Boolean,
            default: false
        },
        email_verified: {
            type: Boolean,
            default: false
        },
        blocked: {
            type:Boolean,
            default:false
        },
        active: {
            type:Boolean,
            default:false
        },
        loans_count: {
            type: Number,
            default: 0
        },
        total_loans_taken: {
            type: Number,
            default: 0
        },
        loans_failed: {
            type: Number,
            default: 0
        },
        loans_successful: {
            type: Number,
            default: 0
        },
        total_loan_paid: {
            type: Number,
            default: 0
        },
        current_loan: {
            type: Number,
            default: 0
        },
        current_loan_id: {
            type: mongoose.Schema.Types.ObjectId,
            default: null,
            ref: 'loans'
        },
        control:{
            type:String,
            enum:[
                'customer',
                'employer'
            ]
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    }
)





export const customerDb = mongoose.model('customers',schema);