import { formatePhonenumber } from "../../../modules/formate-phone-number";


export const sendOnboardAlert = async (request,customers) => {
    const message = (password:string, firstname="") =>{
        return 'Hello ' + firstname
            + '\n'
            + 'You have been onboarded on PayGenie. '
            + 'Kindly use this PIN ' + password + ' and your Phone Number to '
            + 'activate your account. '
            + 'Download App on the Store or login via https://app.mypaygenie.com.'
            + '\n'
            + 'Thank you for using PayGenie.';
    }
    if (Array.isArray(customers)) {
        customers.forEach(customer=>{
            if(customer._id) { 
                const {password,email,mobile,firstname}=customer;           
                request.server.methods.nsanoSendSMS(
                    {
                        type:'single',
                        data:{
                            recipient: formatePhonenumber(mobile),
                            message: message(password,firstname)
                        }
                    }
                ).catch(err=>null);
                request.server.methods.SendEmail(
                    {
                        from:'info@mypaygenie.com', // sender address
                        to:email, // list of receivers
                        subject: 'ONBOARDING ALERT', // Subject line
                        html: `
                            <p>${message(password)}</p>
                            <p>Thank you for using Paygenie</p>
                            `
                    }
                ).catch(err=>null);
            }
        })
    } else {
        const {password,email,mobile,firstname}=customers;           
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(mobile),
                    message: message(password,firstname)
                }
            }
        ).catch(err=>null);
        request.server.methods.SendEmail(
            {
                from:'info@mypaygenie.com', // sender address
                to: email, // list of receivers
                subject: 'ONBOARDING ALERT', // Subject line
                html: `
                    <p>${message(password,firstname)}</p>
                    <p>Thank you for using Paygenie</p>
                    `
            }
        ).catch(err=>null);
    }

    return;
    
}