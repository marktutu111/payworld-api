import * as bcrypt from "bcrypt";
import { sendOnboardAlert } from "./customers.sendalertonboard";
import * as mongoose from "mongoose";
import { formatePhonenumber } from "../../../modules/formate-phone-number";
import * as randomatic from "randomatic";


const NewCustomer = async (request,h) => {
    try {

        let {
            email,
            mobile,
            empcode,
            empId,
            control
        } = request.payload;

        let employer:any={};
        let validEmployer=mongoose.Types.ObjectId.isValid(empId);

        if (!validEmployer && empId !== 'other'){
            throw Error('Employer type is not known');
        }

        if (validEmployer){
            employer = await request.server.methods.dbEmployer.findOne(
                {
                    _id:empId
                }
            );
    
            if (!employer || !employer._id) {
                throw Error(
                    'Employer Account is not available'
                );
            }

            const found=await request.server.methods.dbCustomers.findOne(
                {
                    empId:employer['_id'],
                    empcode:employer['empcode'],
                    $or:[
                        {
                            email:email
                        },
                        {
                            mobile:mobile
                        }
                    ]
                }
            )
    
            if (found && found._id){
                throw Error('Email or Phone number has already been used.');
            }

        };

        if(!validEmployer){
            employer=await request.server.methods.dbEmployer.create(
                {
                    name:empcode,
                    blocked:true,
                    status:'pending approval'
                }
            );
            if(!employer || !employer._id){
                throw Error('Dear customer, Your employer could not be verified, please contact customer support.');
            }
        }

        let message:string = 'Customer Account has been Created Succesfully, please login to continue.';
        const customer = await request.server.methods.dbCustomers.create(
            {
                ...request.payload,
                empId:employer['_id'],
                empcode:employer['empcode']
            }
        );

        if (!customer || !customer._id){
            throw Error(
                'Sorry we could not create your account'
            );
        }

        if (control==='employer') {
            const _customer=await request.server.methods.dbCustomers.findOneAndUpdate(
                {
                    _id:customer['_id']
                },
                {
                    password:randomatic('0',5)
                },{ new:true }
            ).catch(err=>null)
            message = 'Your Employee has been onboarded successfully. Login credentials has been sent to your employee to activate account.';
            sendOnboardAlert(request,_customer).catch(err=>null);
        }

        if (employer && employer._id){
            employer.update(
                {
                    $inc:{
                        total_customers:1
                    }
                }
            ).catch(err=>null);
        }

        return h.response(
            {
                success: true,
                message: message,
                data: customer
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}



const bulkCustomers = async (request,h) => {
    try {

        const {
            customers,
            id
        } = request.payload;

        const employer = await request.server.methods.dbEmployer.findOne(
            {
                $or:[
                    {
                        _id:id
                    },
                    {
                        empcode:id
                    }
                ],
                blocked:false
            }
        );

        if (!employer || !employer._id) {
            throw Error('Employer not available');
        }

        let failed: any[] = [];
        let success: any[] =[];

        customers.forEach(async (customer,i) =>{
            const {empcode,email,mobile}=customer;
            try {
                const found = await request.server.methods.dbCustomers.findOne(
                    {
                        empcode: empcode,
                        $or: [
                            {
                                email: email
                            },
                            {
                                mobile: mobile
                            }
                        ]
                    }
                ); 
                found && found['_id'] ? failed[i]=customer : success[i]=customer;
            } catch (err) {
                failed[i]=customer;
            }
        });

        const results = await request.server.methods.dbCustomers.create(success);
        if (results.length > 0) {
            sendOnboardAlert(request,results).catch(err=>null);
        }
        
        return h.response(
            {
                success: true,
                message: 'processing request',
                data: {}
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const approveCustomer=async(request,h)=>{
    try {
        const {id,approved}=request.payload;
        const customer=await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id:id,
                blocked:false
            },
            {
                $set:{
                    approved:approved
                }
            },{ new:true }
        )
        if(!customer || !customer._id){
            throw Error('Customer could not be approved, Please contact admin.');
        }

        let message:string='';
        let response:string='';
        switch (customer.approved) {
            case true:
                response='Customer account approved successfully';
                message='Hello ' + customer.firstname + '!'
                    + '\n'
                    + 'Congratulations, your account has been approved by your employer and '
                    + 'you can now request a loan.'
                    + '\n'
                    + 'Thank you for using PayGenie.';
                break;
            default:
                response='Your approval status has been revoked successfully';
                message='Hello ' + customer.firstname + '!'
                    + '\n'
                    + 'Your approval status has been revoked by your employer. '
                    + 'You can no longer borrow money on PayGenie.'
                    + '\n'
                    + 'We are sorry, Kindly contact your employer.';
                break;
        }
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(customer.mobile),
                    message: message
                }
            }
        ).catch(err=>null);
        return h.response(
            {
                success:true,
                message:response,
                data:customer
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const UpdateCustomer = async (request,h) => {
    try {

        const {
            id,
            data
        } = request.payload;

        let valid = {};
        Object.keys(data).forEach(key => {
            if (key && data[key] !== '') {
                valid[key]=data[key];
            }
        });

        const customer = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set:{
                    ...valid,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        ).populate('current_loan_id');

        if (!customer || !customer._id){
            throw Error('Oops!, Account not found')
        }

        return h.response(
            {
                success: true,
                message: 'Customer updated successfully',
                data: customer
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}




const DeleteCustomer = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let results = await request.server.methods.dbCustomers.findOneAndRemove(
            {
                _id: id
            }
        );

        return h.response(
            {
                success: false,
                message: 'Customer deleted',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



const getCustomers = async (request,h) => {
    try {

        const results = await request.server.methods.dbCustomers.find(
            {}
        ).limit(1000).sort({ createdAt: -1 }).populate('empId');
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}



const getCustomerById = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let customer = await request.server.methods.dbCustomers.findOne(
            {
                $or: [
                    {
                        email: id
                    },
                    {
                        fullname: id
                    },
                    {
                        mobile: id
                    },
                    {
                        _id: id
                    }
                ]
            }
        ).populate('current_loan_id').populate('empId');
        if (customer && customer._id) {
            if (mongoose.Types.ObjectId.isValid(customer['empId'])){
                await customer.populate('empId').execPopulate();
            }
            return h.response(
                {
                    success: true,
                    message: 'success',
                    data: customer
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}




const getCustomer = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let results = await request.server.methods.dbCustomers.findOne(
            {
                $or: [
                    {
                        email: id
                    },
                    {
                        fullname: id
                    },
                    {
                        mobile: id
                    }
                ]
            }
        ).populate('empId');
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



const FilterCustomer = async (request,h) => {
    try {

        const filter  = request.payload;
        const results = await request.server.methods.dbCustomers.find(
            filter
        ).limit(1000).sort({ createdAt: -1 });
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const SearchCustomer = async (request,h) => {
    try {

        let text = request.query;
        const results = await request.server.methods.dbCustomers.find(
            { $text: { $search : text } }
        ).sort({ createdAt: -1 }).limit(1000);
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



const ActivateAccount=async(request,h)=>{
    try {
        const {account,otp,empId}=request.payload;
        const customer =await request.server.methods.dbCustomers.findOne(
            {
                mobile:account,
                empId:empId,
                blocked:false
            }
        );
        if (customer && customer._id){

            const {password}=customer;
            const SALT = await bcrypt.genSalt(10);
            const _hash = await bcrypt.hash(password, SALT);
            const verifyOtp = await request.server.methods.dbOtp.findOne(
                {
                    account: account,
                    otp: otp
                }
            )

            if (!verifyOtp || !verifyOtp['_id']) {
                throw Error('OTP does not match');
            }

            await customer.update(
                {
                    password: _hash,
                    active:true
                }
            )
            return h.response(
                {
                    success:true,
                    message:'Account activated succesfully',
                    data:customer
                }
            )
        } throw Error('Account not found or maybe blocked');
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};



const getLoginAccounts=async(request,h)=>{
    try {
        const {phone}=request.payload;
        const results=await request.server.methods.dbCustomers.find(
            {
                blocked:false,
                $or:[
                    {
                        email:phone
                    },
                    {
                        mobile:phone
                    }
                ]
            }
        ).populate('empId');
        if(results.length<=0){
            throw Error('Account not found!');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const LoginHandle = async (request, h) => {
    try {

        const { 
            email, 
            password,
            empId
        } = request.payload;
        const customer = await request.server.methods.dbCustomers.findOne(
            { 
                empId:empId,
                blocked:false,
                $or: [
                    {
                        email:email
                    },
                    {
                        mobile:email
                    }
                ],
            }
        ).populate('current_loan_id').populate('empId');
        if (!customer || !customer._id){
            throw Error('Account not found');
        }
        const {active}=customer;
        switch (active) {
            case true:
                const _match = await bcrypt.compare(password, customer['password']);
                if (_match) {
                    const token: string = request.server.methods.generateJWT(customer.toObject(), '1 year');
                    return h.response(
                        {
                            success: true,
                            message: 'success',
                            data: customer,
                            token: token
                        }
                    )
    
                } throw Error('Pin is not valid for account');
            default:
                if (password === customer.password) {
                    return h.response(
                        {
                            success:true,
                            message:'account pending',
                            data:customer
                        }
                    )
                } throw Error(`Pin is not valid for account`);
        }
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        );

    }
}




const setLastSeen = async (request,h) => {
    try {

        const {
            id
        } = request.payload;
        const results = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    last_seen: Date.now()
                }
            }
        )

        return h.response(
            {
                success: true,
                message: 'last seen successfull',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}





const delete_customer_message = async (request,h) => {
    try {

        const {
            customerId,
            id
        } = request.params;
        const results = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id: customerId
            },
            {
                $pull: {
                    messages: {
                        _id: id
                    }
                }
            }
        );

        return h.response(
            {
                success: true,
                message: 'deleted',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}





const add_new_customer_message = async (request,h) => {
    try {

        const {
            CustomerId,
            data
        } = request.params;

        const results = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id: CustomerId
            },
            {
                $push: {
                    messages: data
                }
            }
        );

        return h.response(
            {
                success: true,
                message: 'deleted',
                data: results
            }
        )
        
    } catch (err) {

        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}




const get_customer_summary = async (request,h) => {
    try {

        const results = await request.server.methods.dbCustomers.aggregate(
            [
                {
                    $group: {
                        _id: null,
                        totalBlocked: { $count: "$blocked" },
                        count: { $sum: 1 } 
                    }
                }
            ]
        )

        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )

        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}




const resetPassword = async (request,h) => {
    try {

        const {
            account,
            otp,
            password,
            empId
        } = request.payload;

        const SALT = await bcrypt.genSalt(10);
        const _hash = await bcrypt.hash(password, SALT);
        const verifyOtp = await request.server.methods.dbOtp.findOne(
            {
                account: account,
                otp: otp
            }
        )
        if (!verifyOtp || !verifyOtp['_id']) {
            throw Error('OTP does not match');
        }
        const customer = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                empId:empId,
                blocked: false,
                $or: [
                    {
                        email:account
                    },
                    {
                        mobile:account
                    }
                ]
            },
            {
                $set: {
                    password: _hash,
                    active:true
                }
            },
            {
                new:true
            }
        );

        if (customer && customer['_id']) {
            return h.response(
                {
                    success: true,
                    message: 'PIN reset successful',
                    data: customer
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}

const blockCustomer=async(request,h)=>{
    try {
        const {id,blocked}=request.payload;
        const customer=await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{blocked:blocked}
            },{new:true}
        )
        if(!customer || !customer._id){
            throw Error('Request could not be completed');
        }
        let message:string='';
        let response:string='';
        switch (customer.blocked) {
            case true:
                response='Customer account blocked.';
                message="Hello " + customer.firstname + ', '
                    + 'your account has been blocked. '
                    + 'You can no longer use PayGenie, '
                    + 'kindly contact customer support.'
                    + '\n'
                    + 'Thank you for using PayGenie.'
                break;
            default:
                response='customer account activated';
                message="Hello " + customer.firstname + ', '
                    + 'your account has been unblocked, '
                    + 'you can continue to use PayGenie.'
                    + '\n'
                    + 'Welcome to PayGenie.'
                break;
        }
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(customer.mobile),
                    message:message
                }
            }
        ).catch(err=>null);
        return h.response(
            {
                success:true,
                message:response,
                data:customer
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updatePassword = async (request,h) => {
    try {

        const {
            id,
            oldpassword,
            newpassword
        } = request.payload;
        if (oldpassword === newpassword) {
            throw Error('Oops!, old pin cannot be the same as new pin');
        }
        const customer = await request.server.methods.dbCustomers.findOne(
            {
                _id:id,
                blocked:false
            }
        );
        if (customer && customer['_id']) {
            const _match = await bcrypt.compare(oldpassword, customer['password']);
            if (_match) {
                const SALT = await bcrypt.genSalt(10);
                const _hash = await bcrypt.hash(newpassword, SALT);
                await customer.update(
                    {
                        password: _hash
                    }
                );

                return h.response(
                    {
                        success: true,
                        message: 'success',
                        data: null
                    }
                )
            } throw Error('Old pin does not match');
        } throw Error('Customer not available');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



export default {
    NewCustomer,
    UpdateCustomer,
    DeleteCustomer,
    FilterCustomer,
    SearchCustomer,
    resetPassword,
    updatePassword,
    get_customer_summary,
    add_new_customer_message,
    setLastSeen,
    getCustomer,
    getCustomerById,
    getCustomers,
    delete_customer_message,
    LoginHandle,
    bulkCustomers,
    ActivateAccount,
    approveCustomer,
    blockCustomer,
    getLoginAccounts
}