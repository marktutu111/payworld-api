import * as joi from "joi";
import Handler from "../handler/customers.handle";


export const routes = [
    {
        path: '/new',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    control:joi.string().optional().default('customer').valid(
                        [
                            'customer',
                            'employer'
                        ]
                    ),
                    firstname: joi.string().required(),
                    lastname: joi.string().required(),
                    maxnet: joi.string().required(),
                    empcode: joi.string().optional().allow(
                        [
                            '',
                            null
                        ]
                    ),
                    empId: joi.string().optional(),
                    account_issuer:joi.string().optional(),
                    approved:joi.boolean().optional().default(false),
                    password: joi.string().optional().allow(
                        [
                            null,
                            ''
                        ]
                    ).length(5).error(new Error('Invalid Pin')),
                    gender: joi.string().required().valid(
                        [
                            'MALE',
                            'FEMALE',
                            'Male',
                            'Female',
                            'male',
                            'female'
                        ]
                    ),
                    email: joi.string().email().required().error(new Error('Email is not valid')),
                    mobile: joi.string().required().length(10).error(new Error('Phone number must be 10 digits')),
                    dob: joi.string().required(),
                }
            }
        },
        handler: Handler.NewCustomer
    },
    {
        path: '/new/bulk',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    customers: joi.array().required()
                }
            }
        },
        handler: Handler.bulkCustomers
    },
    {
        path: '/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object().allow()
                }
            },
            handler: Handler.UpdateCustomer
        }
    },
    {
        path:'/approve',
        method:'POST',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    approved:joi.boolean().required()
                }
            }
        },
        handler:Handler.approveCustomer
    },
    {
        path: '/last-seen',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required()
                }
            },
            handler: Handler.setLastSeen
        }
    },
    {
        path:'/activate',
        method:'PUT',
        config:{
            validate:{
                payload:{
                    account:joi.string().required(),
                    empId:joi.string().required(),
                    otp:joi.string().required()
                }
            }
        },
        handler:Handler.ActivateAccount
    },
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getCustomers
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            },
            handler: Handler.getCustomerById
        }
    },
    {
        path: '/get/user/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            },
            handler: Handler.getCustomer
        }
    },
    {
        path: '/search',
        method: 'GET',
        config: {
            validate: {
                query: {
                    text: joi.string().required()
                }
            },
            handler: Handler.SearchCustomer
        }
    },
    {
        path: '/filter',
        method: 'POST',
        handler: Handler.FilterCustomer
    },
    {
        path: '/delete/{id}',
        method: 'DELETE',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            },
            handler: Handler.DeleteCustomer
        }
    },
    {
        path: '/login',
        method: 'POST',
        config: {
            auth: false,
            validate: {
                payload: {
                    email: joi.string().required().error(new Error('Email is not valid')),
                    password: joi.string().required().length(5).error(new Error('Invalid Pin')),
                    empId:joi.string().required()
                }
            },
            handler: Handler.LoginHandle
        }
    },
    {
        path:'/login/get',
        method:'POST',
        config:{
            validate:{
                payload:{
                    phone:joi.string().required().length(10)
                }
            }
        },
        handler:Handler.getLoginAccounts
    },
    {
        path: '/messages/new',
        method: 'PUT',
        config: {
            validate: {
                payload: {
                    userId: joi.string().required(),
                    data: joi.object()
                }
            },
            handler: Handler.add_new_customer_message
        }
    },
    {
        path: '/messages/delete/{userId}/{id}',
        method: 'DELETE',
        config: {
            validate: {
                params: {
                    userId: joi.string().required(),
                    id: joi.string().required()
                }
            },
            handler: Handler.delete_customer_message
        }
    },
    {
        path: '/get/summary/data',
        method: 'GET',
        handler: Handler.get_customer_summary
    },
    {
        path: '/reset-password',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    account: joi.string().required(),
                    otp: joi.string().required(),
                    empId:joi.string().required(),
                    password: joi.string().required().length(5).error(new Error('Invalid Pin'))
                }
            },
            handler: Handler.resetPassword
        }
    },
    {
        path: '/update-password',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    oldpassword: joi.string().required().length(5).error(new Error('Invalid Pin')),
                    newpassword: joi.string().required().length(5).error(new Error('Invalid Pin'))
                }
            },
            handler: Handler.updatePassword
        }
    },
    {
        path:'/block',
        method:'POST',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    blocked:joi.boolean().required()
                }
            }
        },
        handler:Handler.blockCustomer
    }
]