import * as mongoose from "mongoose";
import * as randomize from "randomatic";
import * as moment from "moment";

const schema = new mongoose.Schema(
    {
        empcode: {
            type: String,
            default: `PLC${randomize('0',6)}`
        },
        name: {
            type: String,
            default:null
        },
        email:{
            type:String,
            default:null
        },
        category: {
            type: String,
            default: 'FINANCE'
        },
        mobile: {
            type: String,
            default:null
        },
        digital_address: {
            type: String,
            default:null
        },
        address: {
            type: String,
            default:null
        },
        location: {
            type: String,
            default:null
        },
        // This is payment from penalty settled by customer on invoice.
        penalty_accrued:{
            type:Number,
            default:0
        },
        password: {
            type: String,
            default:randomize('0',5)
        },
        blocked: {
            type:Boolean,
            default:false
        },
        total_customers: {
            type: Number,
            default:0
        },
        total_loan_count: {
            type: Number,
            default:0
        },
        total_loan_amount:{
            type:Number,
            default:0
        },
        total_loan_due:{
            type:Number,
            default:0
        },
        next_invoice_date:{ // deprecated | use salary payment date
            type:String,
            default:null
        },
        /*
            Salary payment date
            is the date of the month where it is 
            expected that Salaries will be disbursed
        */ 
        salary_payment_date:{
            type:String,
            default:null
        },
        /*
            Salary payment day referes to the days
            ahead that invoice issued must be settled.
        */ 
        salary_payment_day:{
            type:String,
            default:null
        },
        /*
            Invoice payment frequency is the type of payment day i.e days, months, weeks
        */ 
        salary_payment_frequency:{
            type:String,
            default:null
        },
        active:{
            type:Boolean,
            default:false
        },
        status:{
            type:String,
            default:'approved',
            enum:['approved','pending approval']
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    }
)

export const employerSchema = mongoose.model('employer',schema);