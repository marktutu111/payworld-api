import * as bcrypt from "bcrypt";
import { formatePhonenumber } from "../../../modules/formate-phone-number";
import * as randomize from "randomatic";
import * as mongoose from "mongoose";


const addNew = async (request,h) => {
    try {
        const{mobile,email}=request.payload;
        const found = await request.server.methods.dbEmployer.findOne(
            {
                $or:[
                    {
                        mobile:mobile
                    },
                    {
                        email:email
                    }
                ]
            }
        )
        if (found && found['_id']) {
            throw Error('Employer with mobile or email already exist');
        }
        const results = await request.server.methods.dbEmployer.create(request.payload);

        if (!results || !results._id){
            throw Error('Employer account failed');
        }

        const {name,password}=results;
        let message='Hello ' + name + ','
        + '\n'
        + 'Congratulations, your account has been created on PayGenie. '
        + 'Your PIN is ' + password + '. '
        + 'Kindly login with your Phone Number using the link below.'
        + '\n'
        + 'https://app.mypaygenie.com/#/employer-login';
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(results.mobile),
                    message:message
                }
            }
        ).catch(err=>null);
        request.server.methods.SendEmail(
            {
                from:'info@mypaygenie.com',
                to:email,
                subject:'PayGenie Employer Registration',
                html: `
                    <p>${message}</p>
                    <br>
                    <p>Thank you for using PayGenie</p>
                `
            }
        ).catch(err=>null);
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: true,
                message: err.message
            }
        )   
    }
}


const approve=async(request,h)=>{
    try {
        const {id}=request.payload;
        const employer=await request.server.methods.dbEmployer.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...request.payload,
                    password:randomize('0',5),
                    status:'approved',
                    blocked:false
                }
            },
            {new:true}
        )
        if(!employer || !employer._id){
            throw Error('Employer not found!');
        }
        const {name,password,email,mobile}=employer;
        let message='Hello ' + name + ','
        + '\n'
        + 'Congratulations, your account has been created on PayGenie. '
        + 'Your PIN is ' + password + '. '
        + 'Kindly login with your Phone Number using the link below.'
        + '\n'
        + 'https://app.mypaygenie.com/#/employer-login';
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(mobile),
                    message:message
                }
            }
        ).catch(err=>null);
        request.server.methods.SendEmail(
            {
                from:'info@mypaygenie.com',
                to:email,
                subject:'PayGenie Employer Registration',
                html: `
                    <p>${message}</p>
                    <br>
                    <p>Thank you for using PayGenie</p>
                `
            }
        ).catch(err=>null);
        return h.response(
            {
                success:true,
                message:'Employer onboarded successfully',
                data:employer
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deleteEmployer = async (request,h)=>{
    try {
        const {id}=request.params;
        const results = await request.server.methods.dbEmployer.findOneAndRemove(
            {
                _id:id
            }
        )
        if (results && results['_id']) {
            return h.response(
                {
                    success: true,
                    message: 'Delete successful',
                    data: results
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}

const updateEmployer = async (request,h) => {
    try {
        const {
            id,
            data
        } = request.payload;
        const results = await request.server.methods.dbEmployer.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        )
        if (results && results['_id']) {
            return h.response(
                {
                    success: true,
                    message: 'Update successful',
                    data: results
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}



const approveAll=async(request,h)=>{
    try {
        const {id}=request.payload;
        const results=await request.server.methods.dbCustomers.find(
            {
                empId:mongoose.Types.ObjectId(id),
                approved:false
            }
        );
        if(!results[0]){
            throw Error('All Employees have been approved already');
        }
        (async()=>{
            results.forEach(customer=>{
                const message='Hello ' + customer.firstname + '!'
                        + '\n'
                        + 'Congratulations, your account has been approved by your employer and '
                        + 'you can now request a loan.'
                        + '\n'
                        + 'Thank you for using PayGenie.';
                request.server.methods.nsanoSendSMS(
                    {
                        type:'single',
                        data:{
                            recipient:formatePhonenumber(customer.mobile),
                            message: message
                        }
                    }
                ).catch(err=>null);
            })
        })().catch(err=>null);
        return h.response(
            {
                success:true,
                message:`Congratulations! ${results.length} employee(s) have been aprroved. Each
                employee would receive an sms and email notification to start borrowing from
                PayGenie. \nThank you for choosing us.`,
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getEmployer = async (request,h) => {
    try {
        const {id}=request.params;
        const results = await request.server.methods.dbEmployer.findOne(
            {
                _id:id
            }
        )
        if (results._id) {
            return h.response(
                {
                    success:true,
                    message: 'success',
                    data: results
                }
            )
        } throw Error('Account not found');
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const getAll = async (request,h) => {
    try {
        const results = await request.server.methods.dbEmployer.find(
            {}
        ).limit(1000).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const getCustomers = async (request,h) => {
    try {
        const {id}=request.params;
        const results = await request.server.methods.dbCustomers.find(
            {
                $or: [
                    {
                        empId:id
                    },
                    {
                        empcode:id
                    }
                ]
            }
        ).limit(1000).sort({createdAt:-1}).populate('empId');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getCustomerLoans = async (request,h) => {
    try {
        const {id}=request.params;
        const results =await request.server.methods.dbLoans.find(
            {
                employer:id
            }
        ).populate('customer').limit(1000).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const login = async (request,h) => {
    try {
        const {username,password}=request.payload;
        const employer = await request.server.methods.dbEmployer.findOne(
            {
                $or:[
                    {
                        empcode:username
                    },
                    {
                        mobile:username
                    },
                    {
                        email:username
                    }
                ],
                blocked:false
            }
        )
        if (!employer || !employer._id) {
            throw Error('Account not found!');
        }
        const {active}=employer;
        const token: string = request.server.methods.generateJWT(employer.toObject(), '1 year');
        switch (active) {
            case true:
                const _match = await bcrypt.compare(password, employer['password']);
                if (_match) {
                    return h.response(
                        {
                            success:true,
                            message:'success',
                            data:employer,
                            token:token
                        }
                    )
    
                } throw Error('Pin is not valid for account');
            default:
                if (password === employer.password) {
                    return h.response(
                        {
                            success:true,
                            message:'account pending',
                            data:employer,
                            token:token
                        }
                    )
                } throw Error(`Pin is not valid for account`);
        }
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const changePin=async(request,h)=>{
    try {
        const {oldpin,newpin,id}=request.payload;
        if(oldpin===newpin){
            throw Error('Old Pin cannot be the same as new pin');
        }
        const SALT = await bcrypt.genSalt(10);
        const _hash = await bcrypt.hash(newpin,SALT);
        const employer=await request.server.methods.dbEmployer.findOne(
            {
                _id:id
            }
        )
        if(!employer || !employer._id){
            throw Error('Password could not be changed');
        };
        if(employer.active){
            const _match = await bcrypt.compare(oldpin, employer['password']);
            if(!_match){
                throw Error('Old PIN does not match');
            }
        } else {
            if(oldpin!==employer.password){
                throw Error('Old PIN does not match');
            }
        }
        await employer.update(
            {
                password:_hash,
                active:true
            }
        );
        return h.response(
            {
                success:true,
                message:'Password reset successful',
                data:employer
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    addNew,
    updateEmployer,
    deleteEmployer,
    getEmployer,
    getAll,
    getCustomers,
    getCustomerLoans,
    login,
    approve,
    changePin,
    approveAll
}