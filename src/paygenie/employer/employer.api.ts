import { employerSchema } from "./database/employer.db";
import { routes } from "./routes/employer.route";

const employer = {
    pkg: {
        name: 'employer',
        version: '1'
    },
    register: async (server) => {
        server.method('dbEmployer', employerSchema);
        server.route(routes);
    }
}


export default employer;