import * as joi from "joi";
import Handler from "../handler/employer.handler";

export const routes = [
    {
        path: '/new',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    name: joi.string().required(),
                    mobile: joi.string().required().length(10),
                    digital_address: joi.string().required(),
                    address: joi.string().required(),
                    location: joi.string().required(),
                    email:joi.string().email().required()
                }
            }
        },
        handler: Handler.addNew
    },
    {
        path: '/delete/{id}',
        method: 'DELETE',
        config: {
            validate: {
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler: Handler.deleteEmployer
    },
    {
        path:'/approve',
        method:'POST',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    mobile:joi.string().required(),
                    email:joi.string().email().required(),
                    address:joi.string().required(),
                    location:joi.string().required(),
                    digital_address:joi.string().required()
                }
            }
        },
        handler:Handler.approve
    },
    {
        path: '/update',
        method: ['PUT','POST'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object().required()
                }
            }
        },
        handler: Handler.updateEmployer
    },
    {
        path: '/login',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    username: joi.string().required(),
                    password: joi.string().required()
                }
            }
        },
        handler: Handler.login
    },
    {
        path: '/get/customers/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getCustomers
    },
    {
        path: '/get/loans/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getCustomerLoans
    },
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getAll
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getEmployer
    },
    {
        path:'/changepassword',
        method:'PUT',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    oldpin:joi.string().required().length(5),
                    newpin:joi.string().required().length(5)
                }
            }
        },
        handler:Handler.changePin
    },
    {
        path:'/approveall',
        method:'POST',
        config:{
            validate:{
                payload:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.approveAll
    }
]