import { loans } from "./database/loans.db";
import { routes } from "./routes/loans.route";
import callback from "./callback/loans.callback";


const api = {
    pkg: {
        name: 'loans',
        version: '1'
    },
    register:async(server)=>{
        server.method('dbLoans',loans);
        server.method('loanCallback',callback);
        server.route(routes);
    }
}

export default api;