import { processLoanMomo } from "./loan.process-momo";
import { processRepayment } from "./loan.process-repayment";
import * as moment from "moment";
import * as bcrypt from "bcrypt";
import * as mongoose from "mongoose";
import { formatePhonenumber } from "../../../modules/formate-phone-number";
import { LoanModel } from "../../../paygenie/models/loans.model";



const borrow = async (request,h) => {
    try {

        const {
            pin,
            amount,
            account_type
        } = request.payload;

        const _employer=request.payload.employer;
        if (!mongoose.Types.ObjectId.isValid(_employer)){
            throw Error('Sorry, your employer is not registered on PayGenie, We will contact you as and when your employer is onboarded. Thank you for using PayGenie.');
        }

        const settings=await request.server.methods.dbLoanSettings.findOne(
            {
                $or: [
                    {
                        employer:_employer
                    },
                    {
                        type:'default'
                    }
                ]
            }
        );

        if (!settings || !settings._id) {
            throw Error(
                'Loan defaults not set'
            );
        }

        const [employer, customer, onLoan]=await Promise.all(
            [
                request.server.methods.dbEmployer.findOne(
                    {
                        _id:_employer,
                        blocked:false
                    }
                ),
                request.server.methods.dbCustomers.findOne(
                    {
                        _id:request.payload.customer,
                        active:true,
                        empId:_employer,
                        blocked:false
                    }
                ),
                request.server.methods.dbLoans.findOne(
                    {
                        customer:request.payload.customer,
                        status:'pending',
                        paidout:true,
                        
                    }
                ),
            ]
        )

        if (!employer || !employer._id) {
            throw Error('Sorry, Your employer cannot authorize your payment');
        }

        if (!customer || !customer._id){
            throw Error(
                'Transaction failed, Account not valid, we cannot process your request.'
            )
        }

        const {password,approved}=customer;
        const match = await bcrypt.compare(pin,password);
        if (match) {
            if (!approved) {
                throw Error(
                    `Sorry, your Signup account has not been activated by your Employer, you can not request a Loan, please contact your employer.`
                );
            }
        } else {
            throw Error('Sorry, transaction failed for invalid Pin');
        }


        let Loan = new LoanModel(onLoan || {});
        Loan.setBorrowingAmount(amount);

        if (Loan._id) {

            if (Loan.owing()){
                throw Error(
                    'Sorry We cannot process your request, You have not paid previous loan.' 
                )
            }

            if (!Loan.canBorrowExtra()){
                throw(
                    `Sorry, you cannot borrow the amount requested, you have exceeded your lending amount.
                    you can request amounts below Ghs${Loan.canBorrowAmount()}, Thank for using PayGenie.
                    `
                )
            }

            switch (account_type) {
                case 'momo':
                    const response = await processLoanMomo(
                        request,
                        Loan,
                        customer,
                        employer
                    );
                    return h.response(response);
                case 'bank':
                    throw Error('Bank transfers not available at the moment');
                default:
                    throw Error('Account type cannot be processed');
            }
            
        } else {

            const {
                rate,
                duration,
                duration_type,
                penalty,
                borrow_limit
            }=settings;

            const data={
                ...request.payload,
                rate:rate,
                duration:duration,
                duration_type:duration_type,
                penalty:penalty,
                maxLoan:customer.maxnet,
                amount_remaining:Loan.getInterest(rate),
                amountBorrowedWithInterest:Loan.getInterest(rate),
                can_borrow_amount:Loan.borrowLimit(customer.maxnet,borrow_limit),
                next_payment_date: moment().add(duration,duration_type)
            }

            const loan = await request.server.methods.dbLoans.create(data);
            Loan=new LoanModel(loan);
            Loan.setBorrowingAmount(amount);

            if (Loan._id) {
                if (!Loan.canBorrowStart()) {
                    loan.update(
                        {
                            status:'failed'
                        }
                    ).catch(err=>null);
                    throw Error(
                        `Sorry, you cannot borrow the amount requested, 
                        you have exceded your lending amount.
                        you can request amounts below Ghs${Loan.can_borrow_amount}, Thank for using PayGenie.
                        `
                    )
                }
                switch (account_type) {
                    case 'momo':
                        try {
                            const response=await processLoanMomo(
                                request,
                                Loan,
                                customer,
                                employer
                            );
                            return h.response(response);
                        } catch (err) {
                            throw Error(err.message);
                        }
                    case 'bank':
                        throw Error(
                            'Bank transfers not available at the moment'
                        );
                    default:
                        throw Error(
                            'Account type cannot be processed'
                        );
                }
            }
        }
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}



const payLoan = async (request,h) => {
    try {
        const {
            customer,
            employer,
            account_type,
            amount,
            otp,
            account_number
        }=request.payload;

        const customerDetails =await request.server.methods.dbCustomers.findOne(
            {
                _id:customer,
                blocked:false,
                approved:true
            }
        )

        if (!customerDetails || !customerDetails._id) {
            throw Error('Customer is not available on PayGenie');
        }

        const verifyOtp = await request.server.methods.dbOtp.findOne(
            {
                account: account_number,
                otp: otp
            }
        )

        if (!verifyOtp || !verifyOtp['_id']) {
            throw Error('OTP does not match, We cannot process your transaction');
        }

        const loan = await request.server.methods.dbLoans.findOne(
            {
                customer:customer,
                employer:employer,
                status:'pending',
                paidout:true,
            }
        ).populate('customer');

        if (loan && loan._id) {
            if (typeof amount === 'string'){
                if (parseFloat(amount) > loan.amount){
                    request.payload['amount'] = loan.amount_remaining;
                }
            } else{
                if (amount > loan.amount_remaining) {
                    request.payload['amount'] = loan.amount_remaining;
                }
            }

            switch (account_type) {
                case 'momo':
                    const response = await processRepayment(
                        request,
                        loan
                    )
                    return h.response(response);
                default:
                    throw Error('Account type cannot be processed');
            }
        } throw Error('Congratulations, you dont have any loan with us, Kindly request for one.');
    } catch (err) {
        return h.response(
            {
                success:false,
                message: err.message
            }
        )
    }
}


const creditCallback = async (request,h) => {
    try {
        const {
            reference,
            code,
            msg
        }=request.payload;

        const [transaction,cashback] = await Promise.all(
            [
                request.server.methods.dbTransactions.findOneAndUpdate(
                    {
                        callbackRef:reference
                    },
                    {
                        $set: {
                            updatedAt:Date.now(),
                            transaction:request.payload
                        }
                    },
                    { new:true }
                ).populate('customer').populate('loan'),
                request.server.methods.dbLoans.findOneAndUpdate(
                    {
                        cashbackReference:reference
                    },
                    {
                        $set:{
                            cashbackTransaction:request.payload,
                            updatedAt:Date.now()
                        }
                    }
                ).populate('customer')
            ]
        )

        if (cashback && cashback._id){
            const {
                firstname,
                mobile
            }=cashback.customer;
            console.log(firstname,mobile);
            const message = 'Congratulations ' + firstname
            + '\n'
            + 'your loan has been paid by your employer, you payment has been refunded to your mobile money wallet.'
            + '\n'
            + 'Thank you for using PayGenie';
            request.server.methods.nsanoSendSMS(
                {
                    type:'single',
                    data:{
                        recipient:formatePhonenumber(mobile),
                        message:message
                    }
                }
            ).catch(err=>null);
            return h.response(
                {
                    code: '00', 
                    msg: 'response message'
                }
            );
        }

        if (!transaction || !transaction._id){
            throw Error('Transaction not found!');
        }

        if (transaction.transaction_type === 'refund'){
            transaction.update(
                {
                    bill_refund:request.payload
                }
            ).catch(err=>null);
            return h.response(
                {
                    code:'00', 
                    msg:'response message'
                }
            );
        }

        const {customer,loan,amount,account_number}=transaction;
        const Loan = new LoanModel(loan);
        Loan.setBorrowingAmount(amount);

        let status: 'paid' | 'failed' = 'failed';

        switch (code) {
            case '00':
                status = 'paid';
                break;
            default:
                status = 'failed';
                break;
        }
        transaction.update(
            {
                status:status
            }
        ).catch(err=>null);
        const result = Loan.aggregate(status,request.server);
        /* NO LONGER IN USE BECAUSE AGGREGATION IS DONE AND RESULTS RETURNED */ 
        request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id:customer._id
            },
            {
                $inc: {
                    loans_successful:result.loans_successful,
                    loans_failed:result.loans_failed,
                    total_loans_taken:result.total_loans_taken
                },
                $set: {
                    current_loan:result.current_loan,
                    current_loan_id:result.current_loan_id
                }
            }
        ).catch(err=>request.server.methods.dbErrors.create(
            {
                id:transaction._id,
                message:err.message,
                error:err
            }
        ));
        /*  EXIT BLOCK */ 
        let message:string='';
        if (status==='paid') {
            message = 'Hello ' + customer.firstname 
            + '\n'
            + 'Your loan has been processed, '
            + 'You have received Ghs ' + amount + ' '
            + 'from PayGenie.Your payment date is ' 
            + new Date(loan['next_payment_date']).toDateString() + '.'
            + '\n'
            + 'Thank you for using PayGenie.'
        } else {
            message = 'Hello ' + customer.firstname 
            + '\n'
            + 'We could not process your loan request because the '
            + 'transaction failed.'
            + '\n'
            + 'Thank you for using PayGenie.'
        }
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(account_number),
                    message:message
                }
            }
        ).catch(err=>null);
        return h.response(
            {
                code:'00', 
                msg:'response message'
            }
        );

    } catch (err) {
        request.server.methods.dbErrors.create(
            {
                id: request.payload.reference,
                message: err.message,
                error: err
            }
        ).catch(err=>null);
        return h.response(
            {
                code: '00', 
                msg: 'response message'
            }
        )
    }
}




const debitCallback = async (request,h) => {
    try {
        const {
            reference,
        }=request.payload;
        const transaction = await request.server.methods.dbTransactions.findOneAndUpdate(
            {
                callbackRef:reference
            },
            {
                $set: {
                    updatedAt: Date.now(),
                    transaction: request.payload
                }
            },
            {
                new:true
            }
        ).populate('customer').populate('loan').populate('bill');

        if (!transaction || !transaction._id) {
            throw Error('Transaction not found!');
        }

        switch (transaction.service_type) {
            case 'bill':
                return await request.server.methods.billCallback(request,transaction);
            case 'loan':
                return await request.server.methods.loanCallback(request,transaction);
            default:
                throw Error('Service type not availanle');
        }
    } catch (err) {
        request.server.methods.dbErrors.create(
            {
                id: request.payload.reference,
                message: err.message,
                error: err
            }
        ).catch(err=>null);
        return h.response(
            {
                code: '00', 
                msg: 'response message'
            }
        )
    }
}


const getLoans = async (request,h) => {
    try {
        const results = await request.server.methods.dbLoans.find(
            {}
        ).limit(1000).sort({createdAt:-1}).populate('customer').populate('employer');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getLoanId = async (request,h) => {
    try {
        const {id}=request.params;
        const results = await request.server.methods.dbLoans.find(
            {
                $or: [
                    {
                        _id:id
                    },
                    {
                        customer:id
                    },
                    {
                        employer:id
                    }
                ]
            }
        ).limit(1000).sort({createdAt:-1}).populate('customer').populate('employer');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const filterLoans = async (request,h) => {
    try {
        const data=request.payload;
        let valid = {};
        Object.keys(data).forEach(key =>{
            if (key && data[key]) {
                valid[key]=data[key];
            }
        })
        const results = await request.server.methods.dbLoans.find(
            valid
        ).limit(1000).sort({createdAt:-1}).populate('customer').populate('employer');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updateLoan=async(request,h)=>{
    try {
        const {
            id,
            data
        } = request.payload;
        let valid = {};
        Object.keys(data).forEach(key => {
            if (key && data[key] !== '') {
                valid[key]=data[key];
            }
        });
        const results=await request.server.methods.dbLoans.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:valid
            },
            {
                new:true
            }
        );
        if (!results || !results._id){
            throw Error('Update failed');
        }
        return h.response(
            {
                success:true,
                message:'updated',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getLoansDuePayment = async (request,h) => {
    try {
        const results = await request.server.methods.dbLoans.find(
            {
                status:'pending',
                paidout:true,
                next_payment_date: {
                    $lte: new Date().toISOString()
                }
            }
        ).populate('customer').populate('employer').limit(1000).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const filterByDate = async (request,h) => {
    try {
        const {to,from}=request.payload;
        const results = await request.server.methods.dbLoans.find(
            {
                createdAt:{
                    $gte:new Date(from).toISOString(),
                    $lte:new Date(to).toISOString()
                }
            }
        ).populate('customer').populate('employer').sort({createdAt:-1}).limit(1000);
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};



const empSummary=async(request,h)=>{
    try {
        const {id}=request.params;
        if(!mongoose.Types.ObjectId.isValid(id))throw Error('Invalid Id');
        const results= await request.server.methods.dbLoans.aggregate(
            [
                { 
                    $match: {
                        employer:mongoose.Types.ObjectId(id),
                        $or:[
                            {
                                status:'pending'
                            },
                            {
                                status:'paid'
                            }
                        ]
                    } 
                },
                {
                    $group: {
                        _id: null,
                        amountBorrowedWithInterest: { $sum: "$amountBorrowedWithInterest" },
                        amountBorrowed: { $sum: "$amount" },
                        amountRemaining:{$sum:"$amount_remaining"},
                        amountPaid:{$sum:"$amount_paid"},
                        count: { $sum: 1 },
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                messagge:err.message
            }
        )
    }
}



const customerSummary=async(request,h)=>{
    try {
        const {id}=request.params;
        if(!mongoose.Types.ObjectId.isValid(id))throw Error('Invalid Id');
        const results= await request.server.methods.dbLoans.aggregate(
            [
                {
                    $match: {
                        customer:mongoose.Types.ObjectId(id),
                        $or:[
                            {
                                status:'pending'
                            },
                            {
                                status:'paid'
                            }
                        ]
                    }
                },
                {
                    $group: {
                        _id: null,
                        amountBorrowedWithInterest: { $sum: "$amountBorrowedWithInterest" },
                        amountBorrowed: { $sum: "$amount" },
                        amountPaid:{$sum:"$amount_paid"},
                        amountRemaining:{$sum:"$amount_remaining"},
                        count: { $sum: 1 },
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                messagge:err.message
            }
        )
    }
}




export default {
    payLoan,
    creditCallback,
    debitCallback,
    getLoans,
    getLoanId,
    filterByDate,
    filterLoans,
    getLoansDuePayment,
    borrow,
    updateLoan,
    empSummary,
    customerSummary
}