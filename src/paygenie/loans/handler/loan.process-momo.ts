import { LoanModel } from "../../../paygenie/models/loans.model";
import { formatePhonenumber } from "../../../modules/formate-phone-number";


export const processLoanMomo = (request,Loan:LoanModel,customer,employer): Promise<any> =>new Promise(async (resolve,reject) => {
    try {

        const loan=Loan.getLoan();
        const {account_issuer,account_number,amount}=request.payload;
        
        const trans = {
            loan: loan._id,
            customer: customer._id,
            account_issuer: account_issuer,
            account_number: account_number,
            account_type: 'momo',
            transaction_type: 'credit',
            amount: amount
        }

        const transaction = await request.server.methods.dbTransactions.create(trans);

        const payload = {
            kuwaita: 'mikopo',
            amount: amount.toString(),
            mno: account_issuer,
            refID: transaction._id,
            msisdn: account_number
        }

        const response=await request.server.methods.nsanoPay(payload);
        await transaction.update(
            {
                callbackRef:response.reference,
                transaction: response,
                updatedAt: Date.now()
            }
        ).catch(err=>request.server.methods.dbErrors.create(
            {
                id: transaction._id,
                message: err.message,
                error: err
            }
        ));

        if (response.code === '03'){
            return resolve(
                {
                    success: true,
                    message: 'Your transaction is received, you will receive an alert when payment is made.',
                    data: transaction
                }
            )
        } else if (response.code === '00'){
            const result=Loan.aggregate('paid',request.server);
            transaction.update(
                {
                    status:'paid',
                }
            ).catch(err=>null);
            customer.update(
                {
                    $inc: {
                        loans_successful:result.loans_successful,
                        loans_failed:result.loans_failed,
                        total_loans_taken:result.total_loans_taken
                    },
                    $set: {
                        current_loan:result.current_loan,
                        current_loan_id:result.current_loan_id
                    }
                }
            ).catch(err=>null);
            employer.update(
                {
                    $inc: {
                        total_loan_amount:result.total_loans_taken,
                        total_loan_due:result.amount_remaining
                    },
                    $set: {
                        updatedAt:Date.now()
                    }
                }
            ).catch(err=>null);
            let message = 'Hello ' + customer.firstname + '!'
            + '\n'
            + 'Your loan has been processed, '
            + 'You have received Ghs ' + amount + ' '
            + 'from PayGenie.Your payment date is ' 
            + new Date(loan['next_payment_date']).toDateString() + '.'
            + '\n'
            + 'Thank you for using PayGenie.';
            request.server.methods.nsanoSendSMS(
                {
                    type:'single',
                    data:{
                        recipient:formatePhonenumber(account_number),
                        message: message
                    }
                }
            ).catch(err=>null);
            return resolve(
                {
                    success:true,
                    message:message,
                    data:transaction
                }
            )
        } else throw Error('Sorry we could process your loan, Transaction failed');
    } catch (err) {
        customer.update(
            {
                $inc: {
                    loans_failed: 1
                }
            }
        ).catch(err=>null);
        Loan.getLoan().update(
            {
                status:'failed'
            }
        ).catch(err=>null);
        reject(err);
    }
})