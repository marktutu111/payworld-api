
const calculate = (_rate: any,_amount: any): number => {
    let rate: number = parseFloat(_rate);
    let amount: number = parseFloat(_amount);
    let interest = (rate / 100) * amount;
    return interest + amount;
}

export default calculate;