
export const processRepayment = (request,loan) => new Promise(async (resolve,reject) => {
    try {
        const {
            account_number,
            account_issuer,
            amount,
            customer
        }=request.payload;

        const trans = {
            loan: loan._id,
            customer: customer,
            account_issuer: account_issuer,
            account_number: account_number,
            account_type: 'momo',
            transaction_type: 'debit',
            amount: amount
        }

        const transaction = await request.server.methods.dbTransactions.create(trans);

        const payload = {
            kuwaita: 'malipo',
            amount: amount.toString(),
            mno: account_issuer,
            refID: transaction._id,
            msisdn: account_number
        }
        
        const response = await request.server.methods.nsanoPay(payload);
        transaction.update(
            {
                transaction:response,
                callbackRef:response.reference
            }
        ).catch(err => request.server.methods.dbErrors.create(
            {
                id: transaction._id,
                message: err.message,
                error: err
            }
        ));
        let message:string='';
        switch (payload.mno) {
            case 'MTN':
                message="Transaction Pending. Kindly dial *170#, select 6) Wallet, Choose 3) My Approvals and  enter MM PIN to approve payment immediately."
                break;
            default:
                message="Your transaction is received, you will receive a prompt to authorize payment on your phone."
                break;
        }
        switch (response.code) {
            case '00':
                return resolve(
                    {
                        success: true,
                        message:message,
                        data: transaction
                    }
                )
            default:
                transaction.update(
                    {
                        status:'failed'
                    }
                ).catch(err=>null);
                throw Error(response.msg);
        }
    } catch (err) {
        reject(err);
    }
})