import * as joi from "joi";
import Handler from "../handler/loans.handler";

export const routes = [
    {
        path: '/new',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    customer: joi.string().required(),
                    employer: joi.string().required(),
                    amount: joi.string().required(),
                    account_number: joi.string().required(),
                    account_issuer: joi.string().required(),
                    account_type: joi.string().required().valid(
                        [
                            'momo',
                            'bank'
                        ]
                    ),
                    pin: joi.string().required()
                }
            }
        },
        handler:Handler.borrow
    },
    {
        path: '/pay',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    customer: joi.string().required(),
                    employer: joi.string().required(),
                    account_number: joi.string().required(),
                    account_issuer: joi.string().required(),
                    account_type: joi.string().required(),
                    amount: joi.alternatives(
                        [
                            joi.string(),
                            joi.number()
                        ]
                    ).required(),
                    otp:joi.string().required()
                }
            }
        },
        handler: Handler.payLoan
    },
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getLoans
    },
    {
        path: '/filter/date',
        method: 'POST',
        config: {
            validate:{
                payload:{
                    to:joi.string().required(),
                    from:joi.string().required()
                }
            }
        },
        handler:Handler.filterByDate
    },
    {
        path:'/get/due',
        method:'GET',
        handler:Handler.getLoansDuePayment
    },
    {
        path:'/update',
        method:['POST','PUT'],
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    data:joi.object()
                }
            }
        },
        handler:Handler.updateLoan
    },
    {
        path: '/filter',
        method: 'POST',
        config: {
            validate:{
                payload:joi.object().required()
            }
        },
        handler:Handler.filterLoans
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.getLoanId
    },
    {
        path: '/credit/callback',
        method: 'POST',
        config:{
            validate:{
                payload:joi.object()
            }
        },
        handler: Handler.creditCallback
    },
    {
        path: '/debit/callback',
        method: 'POST',
        config:{
            validate:{
                payload:joi.object()
            }
        },
        handler: Handler.debitCallback
    },
    {
        path:'/employer/summary/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.empSummary
    },
    {
        path:'/customer/summary/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.customerSummary
    }
]