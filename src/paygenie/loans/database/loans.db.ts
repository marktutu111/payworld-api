import * as mongoose from "mongoose";

const PERCENTAGE = 0.3;

export const schema = new mongoose.Schema(
    {
        customer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'customers',
            required: true
        },
        count:{
            type:Number,
            default:0
        },
        employer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'employer',
            required: true
        },
        amount:{
            type: Number,
            default: 0
        },
        amountBorrowedWithInterest:{
            type:Number,
            default:0
        },
        amount_paid:{
            type: Number,
            default: 0
        },
        amount_remaining: {
            type: Number,
            set:(v:number)=>v.toFixed(2),
            default: 0
        },
        rate: {
            type: Number,
            default: 0
        },
        penalty: {
            type: Number,
            default: 0
        },
        penalty_amount: {
            type: Number,
            default: 0
        },
        remind_date:{
            type:Date,
            default:Date.now
        },
        next_payment_date: {
            type: Date,
            default: null
        },
        duration: {
            type: String,
            required: true
        },
        duration_type: {
            type: String,
            required: true,
            enum: [
                'weeks',
                'months',
                'days',
                'hours'
            ],
        },
        maxLoan: {
            type: Number,
            default: 0
        },
        can_borrow_amount:{
            type:Number,
            default:0
        },
        canBorrow: {
            type: Boolean,
            default:true
        },
        paidout: {
            type: Boolean,
            default: false
        },
        status: {
            type: String,
            default: null,
            enum: [null,'paid','pending','failed']
        },
        defaulted: {
            type: Boolean,
            default: false
        },
        last_borrow_date:{
            type:Date,
            default:null
        },
        date_loan_paid:{
            type:Date
        },
        cashbackAmount:{
            type:String,
        },
        cashbackTransaction:{
            type:Object
        },
        cashbackReference:{
            type:String
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    }
)

export const loans = mongoose.model('loans',schema);