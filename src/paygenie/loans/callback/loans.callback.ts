import { formatePhonenumber } from "../../../modules/formate-phone-number";

const callback=async(request,transaction)=>{
    try {

        const {
            code,
            msg
        }=request.payload;

        const {amount,customer,loan,account_number}=transaction;

        let status: 'paid' | 'failed' = 'failed';
        let message: string = '';
        let amount_paid:number=0;

        switch (code) {
            case '00':
                status = 'paid';;
                amount_paid=amount;
                break;
            default:
                status = 'failed';
                break;
        }

        transaction.update(
            {
                status: status
            }
        ).catch(err=>null);
        const loanUpdate = await request.server.methods.dbLoans.findOneAndUpdate(
            {
                _id:loan._id,
            },
            {
                $inc: {
                    amount_remaining:-amount_paid,
                    amount_paid:amount_paid
                },
                $set: {
                    updatedAt: Date.now()
                }
            },
            {new:true}
        );

        const {amount_remaining}=loanUpdate;
        request.server.methods.dbEmployer.findOneAndUpdate(
            {
                _id:customer.empId
            },
            {
                $inc: {
                    total_loan_due:-amount_paid
                },
                $set: {
                    updatedAt:Date.now()
                }
            }
        ).catch(err=>null);
        const customerUpdate = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id:customer._id
            },
            {
                $inc: {
                    total_loan_paid:amount_paid
                },
                $set: {
                    current_loan:amount_remaining,
                    updatedAt:Date.now()
                }
            }
        );

        if (status==='paid') {
            if (Math.round(amount_remaining) <= 0) {
                message = 'Congratulations, ' + customer.firstname + '! '
                + '\n'
                + 'you have paid your loan, you can now request for a new one.'
                + '\n'
                + 'Thank you for using PayGenie';

                loan.update(
                    {
                        status: 'paid',
                        canBorrow:false,
                        amount_remaining:0,
                        date_loan_paid:Date.now()
                    }
                ).catch(err=>null);
                customerUpdate.update(
                    {
                        current_loan_id: null,
                        current_loan:0
                    }
                ).catch(err=>null);
            } else {
                message = 'Hello ' + customer.firstname + ' '
                + '\n'
                + 'Your loan has been processed '
                + 'your next payment date is' + new Date(loan['next_payment_date']).toDateString() + '.'
                + '\n'
                + 'thank you for using PayGenie'
            }
        }

        if (status==='failed') {
            message = 'Hello ' + customer.firstname +'! '
            + '\n'
            + 'We could not process your loan request, '
            + 'the Transaction failed.'
            + '\n'
            + 'thank you for using PayGenie'
        }
        
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(account_number),
                    message: message
                }
            }
        ).catch(err=>null);
        return {code: '00', msg: 'response message'};
    } catch (err) {
        request.server.methods.dbErrors.create(
            {
                id: request.payload.reference,
                message: err.message,
                error: err
            }
        ).catch(err=>null);
        return (
            {
                code: '00', 
                msg: 'response message'
            }
        )
    }
}


export default callback;