import { errors } from "./database/errors.db";
import { routes } from "./routes/errors.route";

const err = {
    pkg: {
        name: 'errors',
        version: '1'
    },
    register: async (server) => {
        server.method('dbErrors',errors);
        server.route(routes);
    }
}

export default err;