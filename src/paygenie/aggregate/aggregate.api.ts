import { route } from "./route/aggregate.route";

const api={
    pkg:{
        name:'aggregator',
        version:'1'
    },
    register:async(server)=>{
        server.route(route);
    }
}


export default api;