
const handle=async(request,h)=>{
    try {
        const [loans,customers,employers]=await Promise.all(
            [
                request.server.methods.dbLoans.aggregate(
                    [
                        {
                            $match:{}
                        },
                        {
                            $group:{
                                _id: null,
                                total_amount_borrowed: {
                                    $sum: "$amount" 
                                },
                                total_owing:{
                                    $sum:'$amount_remaining'
                                },
                                amount_paid:{
                                    $sum:'$amount_paid'
                                },
                                count:{
                                    $sum:1
                                }
                            }
                        }
                    ]
                ),
                request.server.methods.dbCustomers.aggregate(
                    [
                        {
                            $match:{}
                        },
                        {
                            $group:{
                                _id: null,
                                count:{
                                    $sum:1
                                }
                            }
                        }
                    ]
                ),
                request.server.methods.dbEmployer.aggregate(
                    [
                        {
                            $match:{}
                        },
                        {
                            $group:{
                                _id:null,
                                count:{
                                    $sum:1
                                }
                            }
                        }
                    ]
                )
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:{
                    loans:loans,
                    customers:customers,
                    employers:employers
                }
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    handle
}