import Handler from "../handler/aggregate.handler";

export const route=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.handle
    }
]