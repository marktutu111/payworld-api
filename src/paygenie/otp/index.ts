import { OTP } from "./db/otp.db";
import { route } from "./route/otp.route";
import { processOtp } from "./process/otp.process";

const plugin = {
    pkg: {
        name: 'otp',
        version: '1'
    },
    register: async (server) => {
        server.method('dbOtp', OTP);
        server.method('sendOTP',processOtp);
        server.route(route);
    }
}

export default plugin;