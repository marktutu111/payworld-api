import * as joi from "joi";
import Handler from "../handle/otp.handle";

export const route = [
    {
        path: '/sendotp',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    account: joi.string().required()
                }
            }
        },
        handler: Handler.sendOtp
    },
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getOtp
    }
]