import * as randomize from "randomatic";
import { formatePhonenumber } from "../../../modules/formate-phone-number";

export const processOtp = (request,account:string) =>new Promise(async (resolve,reject) => {
    try {
        const otp = randomize('0',5);
        const customer = await request.server.methods.dbCustomers.findOne(
            {
                $or: [
                    {
                        email:account
                    },
                    {
                        mobile:account
                    }
                ]
            }
        )

        const response=await request.server.methods.dbOtp.findOneAndUpdate(
            {
                account: account
            },
            {
                $set: {
                    otp: otp
                }
            },
            {
                upsert: true,
                new: true
            }
        );

        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient: formatePhonenumber(account),
                    message: 'Your OTP is ' + otp
                }
            }
        ).catch(err=>null);

        if (customer && customer._id) {
            const {email,firstname}=customer;
            request.server.methods.SendEmail(
                {
                    from: 'info@mypaygenie.com', // sender address
                    to: email, // list of receivers
                    subject: 'PayGenie OTP', // Subject line
                    html: `
                        <b>Hello ${firstname},</b>
                        <p>Your OTP is ${otp}</p>
                        <br>
                        <p>Thank you for using PayGenie</p>
                    `
                }
            ).catch(err=>console.log(err));
        }

        resolve(response);
    } catch (err) {
        reject(err);
    }
});