import { processOtp } from "../process/otp.process";

const sendOtp = async (request,h) => {
    try {
        const {account} = request.payload;
        const response = await processOtp(request,account);
        return h.response(
            {
                success: true,
                message: 'success',
                data: response
            }
        )

    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}


const getOtp = async (request,h) => {
    try {
        const results = await request.server.methods.dbOtp.find({});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    sendOtp,
    getOtp
}