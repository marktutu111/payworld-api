import { services } from "./database/services.db";
import { bills } from "./database/bills.db";
import { route } from "./route/bills.route";
import callback from "./callback/bills.handler";
import paygeniepay from "./handler/bill.paygenie";
import refund from "./refund/bills.refund";


const bill = {
    pkg:{
        name:'bills',
        version: '1'
    },
    register: async (server)=>{
        server.method('dbServices',services);
        server.method('dbBills',bills);
        server.method('billCallback',callback);
        server.method('paygeniepay',paygeniepay);
        server.method('sendBillRefund',refund);
        server.route(route);
    }
}


export default bill;