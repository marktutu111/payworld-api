import * as mongoose from "mongoose";
import { LoanModel } from "../../..//paygenie/models/loans.model";
import * as moment from "moment";
import * as bcrypt from "bcrypt";



const addService = async (request,h) => {
    try {
        const data=request.payload;
        const response =await request.server.methods.dbServices.create(data);
        return h.response(
            {
                success:true,
                message:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getServices = async (request,h) =>{
    try {
        const response=await request.server.methods.dbServices.find(
            {active:true}
        ).limit(1000).sort({createdAt:-1});
        console.log(response);
        return h.response(
            {
                success:true,
                methods:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getList = async(request,h)=>{
    try {
        const payload=request.payload;
        const response = await request.server.methods.nsanoPay(payload);
        if (response.code === '00') {
            const data=response.data.list ? response.data.list:response.data;
            return h.response(
                {
                    success:true,
                    message:'succes',
                    data:data
                }
            )
        } throw Error(response.msg);
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updateService =async (request,h)=>{
    try {
        const {id,data}=request.payload;
        const key = mongoose.Types.ObjectId.isValid(id) ? '_id':'type';
        const response=await request.server.methods.dbServices.findOneAndUpdate(
            {
                [key]:id
            },
            {
                $set:data
            },
            {
                new:true
            }
        )
        if (response && response._id) {
            return h.response(
                {
                    success:true,
                    message:'success',
                    data:response
                }
            )
        } throw Error('Service not found');
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const payBill = async(request,h)=>{
    try {
        const {
            customer,
            sid,
            otp,
            name,
            serviceDetails,
            account_issuer,
            account_number,
            account_type,
            pin
        }=request.payload;

        const {amount}=serviceDetails;
        const customerDetails=await request.server.methods.dbCustomers.findOne(
            {
                _id:customer,
                blocked:false
            }
        )

        if (!customerDetails || !customerDetails._id){
            throw Error('Sorry we cannot process your transaction, Account not found!');
        }

        const service = await request.server.methods.dbServices.findOne(
            {
                sid_buy:sid
            }
        );

        if (!service || !service._id) {
            throw Error('Serice not found!');
        }

        const bill = await request.server.methods.dbBills.create(
            {
                service:service._id,
                name:name,
                serviceDetails:serviceDetails,
                customer:customer,
                sid:sid
            }
        );

        if (!bill && !bill._id){
            throw Error('Oops!, something went wrong, we could process your request');
        }

        if (account_type !== 'paygenie'){

            const verifyOtp = await request.server.methods.dbOtp.findOne(
                {
                    account: account_number,
                    otp: otp
                }
            );
    
            if (!verifyOtp || !verifyOtp['_id']) {
                throw Error('OTP does not match, We cannot process your transaction');
            };

            const trans = {
                bill:bill['_id'],
                serviceId: service._id,
                customer: customer,
                account_issuer: account_issuer,
                account_number: account_number,
                account_type: account_type,
                transaction_type: 'debit',
                service_type:'bill',
                amount: amount
            }
            
            const transaction = await request.server.methods.dbTransactions.create(trans);
            const payload = {
                kuwaita: 'malipo',
                amount: amount.toString(),
                mno: account_issuer,
                refID: transaction._id,
                msisdn: account_number
            };

            const response = await request.server.methods.nsanoPay(payload);
            transaction.update(
                {
                    transaction:response,
                    callbackRef:response.reference
                }
            ).catch(err => request.server.methods.dbErrors.create(
                {
                    id: transaction._id,
                    message: err.message,
                    error: err
                }
            ));
            let message:string='';
            switch (payload.mno) {
                case 'MTN':
                    message="Transaction Pending. Kindly dial *170#, select 6) Wallet, Choose 3) My Approvals and  enter MM PIN to approve payment immediately."
                    break;
                default:
                    message="Your transaction is received, you will receive a prompt to authorize payment on your phone."
                    break;
            }
            switch (response.code) {
                case '00':
                    return h.response(
                        {
                            success: true,
                            message:message,
                            data:transaction
                        }
                    )
                default:
                    transaction.update(
                        {
                            status:'failed'
                        }
                    ).catch(err=>null);
                    bill.update(
                        {
                            status:'failed'
                        }
                    ).catch(err=>null);
                    throw Error(response.msg);
            }
        } else {

            const {password,approved,empId,_id,maxnet}=customerDetails;
            const match = await bcrypt.compare(pin,password);
            if (match) {
                if (!approved) {
                    throw Error(
                        `Sorry, your account has not been activated by your Employer, you can not pay bill with PayGenie, please contact your employer or pay with mobile money.`
                    );
                }
            } else throw Error('Sorry, transaction failed for invalid Pin');


            if (!mongoose.Types.ObjectId.isValid(empId)){
                throw Error('Sorry, your employer is not registered on PayGenie, We will contact you as and when your employer is onboarded. Thank you for using PayGenie.');
            }

            const [employer,settings,loan] = await Promise.all(
                [
                    request.server.methods.dbEmployer.findOne(
                        {
                            _id:empId,
                            blocked:false
                        }
                    ),
                    request.server.methods.dbLoanSettings.findOne(
                        {
                            $or: [
                                {
                                    employer:empId
                                },
                                {
                                    type:'default'
                                }
                            ]
                        }
                    ),
                    request.server.methods.dbLoans.findOne(
                        {
                            customer:_id,
                            status: 'pending',
                            paidout: true,
                        }
                    )
                ]
            )

            if (!employer || !employer._id){
                throw Error('Sorry, your employer has not been registered on PayGenie');
            }

            if (!settings || !settings._id) {
                throw Error('Loan defaults not set');
            }

            let Loan = new LoanModel(loan || {});
            Loan.setBorrowingAmount(amount);

            if (Loan._id) {

                if (Loan.owing()){
                    throw Error(
                        'Sorry We cannot process your request, You have not paid previous loan' 
                    )
                }

                if (!Loan.canBorrowExtra()){
                    throw(
                        `Sorry, you cannot borrow the amount requested, you have exceeded your lending amount.
                        you can request amounts below Ghs${Loan.canBorrowAmount()}, Thank for using PayGenie.
                        `
                    )
                };

                const transaction = await request.server.methods.paygeniepay(
                    request,
                    bill,
                    Loan,
                    employer,
                    customerDetails
                )

                return h.response(transaction);
            
        } else {

                const {
                    rate,
                    duration,
                    duration_type,
                    penalty,
                    percentage
                }=settings;

                const data = {
                    employer:empId,
                    customer:customer,
                    amount:amount,
                    rate:rate,
                    duration:duration,
                    duration_type:duration_type,
                    penalty:penalty,
                    maxLoan:customer.maxnet,
                    amount_remaining: Loan.getInterest(rate),
                    can_borrow_amount:Loan.borrowLimit(maxnet,percentage),
                    next_payment_date: moment().add(duration,duration_type)
                }

                const loan = await request.server.methods.dbLoans.create(data);
                Loan = new LoanModel(loan);

                if (Loan._id) {
                    if (!Loan.canBorrowStart()) {
                        loan.update(
                            {
                                status:'failed'
                            }
                        ).catch(err=>null);
                        throw Error(
                            `Sorry, you cannot borrow the amount requested, 
                            you have exceded your lending amount.
                            you can request amounts below Ghs${Loan.can_borrow_amount}, Thank for using PayGenie.
                            `
                        )
                    }

                    const transaction = await request.server.methods.paygeniepay(
                        request,
                        bill,
                        Loan,
                        employer,
                        customerDetails
                    )
    
                    return h.response(transaction);
                    
                }
            }
        }
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getBills=async(request,h)=>{
    try {
        const results = await request.server.methods.dbBills.find(
            {}
        ).sort({createdAt:-1}).limit(1000);
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getCustomerBills=async(request,h)=>{
    try {
        const {customer}=request.params;
        const results = await request.server.methods.dbBills.find(
            {
                customer:customer
            }
        ).sort({createdAt:-1}).limit(1000);
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    addService,
    getServices,
    getList,
    updateService,
    payBill,
    getBills,
    getCustomerBills
}