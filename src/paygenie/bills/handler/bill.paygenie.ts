import { formatePhonenumber } from "../../../modules/formate-phone-number";
import { LoanModel } from "../../../paygenie/models/loans.model";

const paygeniepay=async(request,bill,Loan:LoanModel,employer,customerDetails)=>{
    try {
        
        const {sid,serviceDetails}=bill;
        const data={
            sid:sid,
            serviceDetails:serviceDetails, 
            kuwaita:'huduma_mkoba'
        }

        const response = await request.server.methods.nsanoPay(data);
        let status:'paid'|'failed'|'pending'=null;

        switch (response.code) {
            case '00':
                status='paid';
                break;
            case '03':
                status='pending';
            default:
                status='failed';
                break;
        }

        bill.update(
            {
                transaction:response,
                status:status
            }
        ).catch(err=>null);

        const {customer,name}=bill;
        let message:string='';

        if (status==='paid') {
            message = 'Hello ' + customerDetails.firstname + '!'
            + '\n'
            + 'You have successfully made payment for service '
            + name + '. '
            + '\n'
            + 'thank you for using PayGenie'; 
        } else{
            message = 'Hello ' + customerDetails.firstname + '!'
            + '\n'
            + 'Sorry, We could not process your service request '
            + 'Transaction failed.'
            + '\n'
            + 'thank you for using PayGenie'; 
        }

        const result = Loan.aggregate(status,request.server);
        customerDetails.update(
            {
                $inc: {
                    loans_successful:result.loans_successful,
                    loans_failed:result.loans_failed,
                    total_loans_taken:result.total_loans_taken,
                    loans_count:1
                },
                current_loan: result.current_loan,
                current_loan_id:Loan._id
            }
        ).catch(err=>null);
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(customerDetails.mobile),
                    message:message
                }
            }
        ).catch(err=>request.server.methods.dbErrors.create(
            {
                id:'SMS | ERROR',
                message:err.message,
                error:err
            }
        ));
        return (
            {
                success:true,
                message:message,
                data:bill
            }
        )
    } catch (err) {
        return (
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default paygeniepay;