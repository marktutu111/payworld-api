import { formatePhonenumber } from "../../../modules/formate-phone-number";

const refund=async(request,transaction)=>{
    try {
        const {
            amount,
            account_issuer,
            account_number,
            customer
        }=transaction;
        await transaction.update(
            { 
                transaction_type:'refund' 
            }
        ).catch(err=>null);
        const payload = {
            kuwaita: 'mikopo',
            amount: amount.toString(),
            mno: account_issuer,
            refID: transaction._id,
            msisdn: account_number
        }

        const response = await request.server.methods.nsanoPay(payload);
        transaction.update(
            {
                bill_refund:response
            }
        ).catch(err=>null);
        let message = 'Hello ' + customer.firstname + '!'
                + '\n'
                + 'Sorry, We could not process your service request. You money has been refunded.'
                + '\n'
                + 'thank you for using PayGenie';
        return request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(customer.mobile),
                    message:message
                }
            }
        ).catch(err=>null);
    } catch (err) {
        request.server.methods.dbErrors.create(
            {
                id:'BILL PAY ERROR',
                message:err.message,
                error:err
            }
        ).catch(err=>null);
    }
}


export default refund;