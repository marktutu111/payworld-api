import * as mongoose from "mongoose";

const schema =new mongoose.Schema(
    {
        name:{
            type:String
        },
        sid_list:{
            type:String,
            unique:true
        },
        type:{
            type:String,
            default:null,
        },
        sid_buy:{
            type:String,
            unique:true
        },
        active:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const services=mongoose.model('services',schema);