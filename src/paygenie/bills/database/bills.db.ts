import * as mongoose from "mongoose";


const schema = new mongoose.Schema(
    {
        customer:{
            type:mongoose.Schema.Types,
            ref:'customers'
        },
        service:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'services'
        },
        sid:{
            type:String
        },
        name:{
            type:String,
            default:null
        },
        serviceDetails:{
            type:Object,
            default:null
        },
        status:{
            type:String,
            enum:[
                'paid',
                'failed'
            ]
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const bills =mongoose.model('bills',schema);

