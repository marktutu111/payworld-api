import { formatePhonenumber } from "../../../modules/formate-phone-number";


const callback = async (request,transaction)=>{
    try {
        const {
            code,
            msg
        }=request.payload;

        if (!transaction || !transaction._id){
            throw Error('Transaction not found!')
        }

        let status: 'paid' | 'failed' = 'failed';
        switch (code) {
            case '00':
                status = 'paid';;
                break;
            default:
                status = 'failed';
                break;
        }

        transaction.update(
            {
                status:status,
                updatedAt: Date.now()
            }
        ).catch(err=>null);

        if (status==='paid') {
            const bill = await request.server.methods.dbBills.findOne(
                {
                    _id:transaction['bill']['_id']
                }
            ).populate('customer');
            if(!bill || !bill._id){
                throw Error('Bill not found!');
            }
            const {sid,serviceDetails}=bill;
            const data={
                sid:sid,
                serviceDetails:serviceDetails, 
                kuwaita:'huduma_mkoba'
            }
            const response = await request.server.methods.nsanoPay(data);
            let status:'paid'|'failed'|'pending'=null;
            switch (response.code) {
                case '00':
                    status='paid';
                    break;
                default:
                    status='failed';
                    break;
            }

            bill.update(
                {
                    transaction:response,
                    status:status
                }
            ).catch(err=>null);

            const {customer,name}=bill;
            const {account_number}=transaction;
            let message:string='';

            if (status==='paid') {
                message = 'Hello ' + customer.firstname + '!'
                + '\n'
                + 'You have successfully made payment for service.'
                + name + '.'
                + '\n'
                + 'thank you for using PayGenie'; 
            } else{
                message = 'Hello ' + customer.firstname + '!'
                + '\n'
                + 'Sorry, We could not process your service request.'
                + '\n'
                + 'thank you for using PayGenie'; 
                request.server.methods.sendBillRefund(
                    request,
                    transaction
                )
            }
            request.server.methods.nsanoSendSMS(
                {
                    type:'single',
                    data:{
                        recipient: formatePhonenumber(account_number),
                        message: message
                    }
                }
            ).catch(err=>request.server.methods.dbErrors.create(
                {
                    id:'SMS | ERROR',
                    message:err.message,
                    error:err
                }
            ));
            return (
                {
                    code: '00', 
                    msg: 'response message'
                }
            )
        }throw Error(msg);
    } catch (err) {
        request.server.methods.dbErrors.create(
            {
                id:'BILL PAY ERROR',
                message:err.message,
                error:err
            }
        ).catch(err=>null);
        return (
            {
                code: '00', 
                msg: 'response message'
            }
        )
    }
}



export default callback;