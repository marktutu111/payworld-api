import * as pdf from "html-pdf";
import { invoice } from "./invoice.html";

const api={
    pkg:{
        name:'invoice',
        version:'1'
    },
    register:async(server)=>{

        const options = {
            width: '50cm',
            height: '29.7cm',
            orientation: "portrait", // portrait or landscape
            border: "1cm",             // default is 0, units: mm, cm, in, px
            // File options
            type: "pdf",             // allowed file types: png, jpeg, pdf
            quality: "75",           // only used for types png & jpeg
        };

        const gem=(html)=>new Promise((resolve,reject)=>{
            pdf.create(html, options).toStream((err, stream) => {
                    if (err) return reject(err);
                    resolve(stream);
                })
        });

        server.route(
            [
                {
                    path:'/get',
                    method:'GET',
                    handler:async(request,h)=>{
                        try {
                            // const pdf=await gem();
                            return h.response('pdf');
                        } catch (err) {
                            return h.response(err.message);
                        } 
                    }
                }
            ]
        );


        server.method('generateInvoice',invoice);
        server.method('createPDFStream',gem);


    }
}

export default api;