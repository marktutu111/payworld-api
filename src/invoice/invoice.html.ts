import * as moment from "moment";


export const invoice=(data,type:'RECIEPT'|'NEW'='NEW')=>(
    ` </html>
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>${type==='NEW'?'INVOICE':'RECIEPT'}</title>
        </head>

        <style>
            .row {
                margin-right:auto;
                margin-left:auto;
                clear: both;
            }
            .col{
                float: left;
            }
            .col-right{
                float:right;
            }
            .h1 {
                font-size: 50px;
                font-weight: bold;
            }
            .move-right{
                margin-right:5%;
            }
            .mv-up{
                margin-top:100px;
                display: block;
            }
            .mv-left{
                margin-left: 30%;
            }
        </style>
        <body style="
            padding: 50px;
        ">

            <div class="row">
                <img width="200px" src="https://app.mypaygenie.com/assets/img/logo2.png">
            </div>
            <div class="row">
                <div class="col">
                    <div class="h1" style="color: rgb(48, 141, 211);">INVOICE</div>
                    <span style="color: rgb(8, 111, 8);font-size: 25px;">${data.employer}</span>
                </div>
                <div class="col-right">
                    <div>Opposite HFC Estates</div>
                    <div>Cambodia Estate</div>
                    <div>Community 18,</div>
                    <div>Tema-Ghana</div>
                </div>
                <div class="col-right move-right">
                    <div style="margin-bottom: 10px;"><b>Invoice Date</b></div>
                    <div>${data.date}</div>
                    <div style="
                        margin-top: 5px;
                        margin-bottom: 5px;
                        color: #000;
                        font-weight: bold;
                    ">Invoice Number</div>
                    <div>${data.invoiceNumber}</div>
                </div>
            </div>
            <div style="
                height: 5px;
                width:100%;
                clear: both;
            "></div>
            <!--  -->
            <div class="row mv-up" style="
                padding: 15px;
            ">
                <div class="col" style="
                    width: 40%;
                ">
                    <div><b>Description</b></div>
                </div>
                <div class="col" style="
                    width: 15%;
                ">
                    <div><b>Qty</b></div>
                </div>
                <div class="col">
                    <div><b>Actual amount</b></div>
                    <div>borrowed <b>GHS</b></div>
                </div>
                <div class="col-right">
                    <div><b>Actual amount</b></div>
                    <div><b>Borrowed + Interest</b></div>
                </div>
            </div>
            <br>
            <div style="
                height: 5px;
                background: #000;
                width:100%;
                clear: both;
            "></div>
            <!--  -->
            <div class="row" style="
                padding: 15px;
            ">
                <div class="col" style="
                    width: 40%;
                ">
                    <div>Monthly transactions for ${moment(data.issueDate || Date.now()).format('MMM YYYY')}</div>
                </div>
                <div class="col" style="
                    width: 15%;
                ">
                    <div>${data.totalLoans}</div>
                </div>
                <div class="col">
                    <div>${data.amountBorrowed}</div>
                </div>
                <div class="col-right">
                    <div>${data.amountBorrowedWithInterest}</div>
                </div>
            </div>
            <br>
            <!--  -->
            <div style="
                height: 2px;
                background: rgb(191, 191, 191);
                width:100%;
                clear: both;
            "></div>
            <!--  -->
            <div class="row" style="
                    padding: 15px;
                ">
                <div class="col-right" 
                    style="
                        width: 100px;
                    "
                >
                    <div>${data.amountBorrowedWithInterest}</div>
                </div>
                <div 
                    class="col-right" 
                    style="
                        width: 100px;
                    "
                >
                    <div>Subtotal</div>
                </div>
            </div>
            <br>
            <!--  -->
            <div style="
                height: 3px;
                background: rgb(50, 50, 50);
                width:20%;
                float:right;
            "></div>
            <!--  -->
            <div class="row" style="
                    padding: 15px;
                ">
                <div class="col-right" 
                    style="
                        width: 100px;
                    "
                >
                    <div>${data.amountBorrowedWithInterest}</div>
                </div>
                <div 
                    class="col-right" 
                    style="
                        width: 100px;
                    "
                >
                    <div>Subtotal</div>
                </div>
            </div>
            <!--  -->
            <div style="
                width:100%;
                clear: both;
            "></div>
            <!--  -->
            ${type==='NEW' ? `<h3 style="margin-top: 100px;">Due date: ${data.due}</h3>`:`<h3 style="margin-top: 100px;">Paid: ${data.datePaid}</h3>`}
        </body>
        </html>
    `
)