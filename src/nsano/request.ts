import * as request from "request";
import { config } from "./config";



export const sendRequest = (data) => {

    console.log(data);
    const { URL, APIKEY } = config;

    let form:string='';
    const keys=Object.keys(data);
    
    keys.forEach((key,i)=>{
        form+=`${key}=${JSON.stringify(data[key]).replace(/^"(.*)"$/, '$1')}`;
        if (keys.length !== i+1){
            form+='&';
        }
    });

    const headers={
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    const options={
        headers:headers,
        body:form
    }
    
    return new Promise((resolve,reject)=>{
        request.post(`${URL}/${APIKEY}`, options, (err, res, bd) => {
            try {
                if (err) reject(err);
                console.log(err,bd)
                resolve(JSON.parse(bd));
            } catch (err) {
                reject(err);    
            }
        });
    });

}


export const sendSmsRequest = (
    {
        type,
        data
    }
) => {

    const { SMS_URL, SMS_KEY } = config;
    const headers = {
        'Content-Type': 'application/json',
        'X-SMS-Apikey':SMS_KEY
    }

    const options = {
        headers: headers,
        body: JSON.stringify({...data,sender:'PayGenie'})
    }
    
    return new Promise((resolve,reject) => {
        request.post(`${SMS_URL}/${type}`, options, (err, res, bd) => {
            try {
                if (err) reject(err);
                resolve(JSON.parse(bd));
            } catch (err) {
                reject(err);    
            }
        });
    })

}