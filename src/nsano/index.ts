import { sendRequest, sendSmsRequest } from "./request";

const nsano = {
    pkg: {
        name: 'nsano',
        version: '1'
    },
    register: async (server) => {
        server.method('nsanoPay',sendRequest);
        server.method('nsanoSendSMS',sendSmsRequest);

        // setTimeout(async() => {
        //     const response=await sendRequest(
        //         {
        //             sid:'BMDB1538684513660',
        //             serviceDetails:{
        //                 "data_bundle_type": "MTNDLY20MB",
        //                 "amount": "0.50",
        //                 "msisdn": "0246824458"
        //             }, 
        //             kuwaita:'huduma_mkoba'
        //         }
        //     ).catch(err=>console.log(err));
        //     console.log(response);
        // }, 1000*3);

        


    }
}

export default nsano;