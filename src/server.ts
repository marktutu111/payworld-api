'use strict';

const Hapi= require('hapi');
import * as Good from "good";
import * as mongoose from "mongoose";


import { goodConfig } from "./config/good.config";
import { _server } from "./config/server.config";
import * as path from "path";


// ROUTES //
import Paygenie from "./paygenie";
import { Db_config } from "./config/database.config";
import ValidateIdExpiry from "./modules/validate-date-expiry";

// NSano
import Nsano from "./nsano";

// email
import EmailClient from "./email-client";

// JWT
import JWT from "./jwt";

// Cron
import LoanCron from "./cron/cron";

// Invoice
import InvoiceAPI from "./invoice/invoice.api";


// Start the server
async function start() {
    try {


        // SERVER CONFIGURATION //
        const server = Hapi.server({
            port: _server.port,
            host: _server.host,
            routes: { 
                cors: { 
                    origin: ['*']
                },
                files: {
                    relativeTo: path.resolve('public')
                }
            }
        });


        // REGISTER SERVER PLUGINS //
        await server.register([
            ValidateIdExpiry,
            JWT,
            {
                plugin: EmailClient,
                routes: {
                    prefix: '/api/email'
                }
            },
            {
                plugin:InvoiceAPI,
                routes:{
                    prefix:'/invoice'
                }
            },
            Nsano,
            Paygenie,
            {
                plugin: Good,
                options: goodConfig
            }
        ]);


        await server.register(LoanCron);


        // CONNECT TO DATABASE
        await mongoose.connect(Db_config.URL, { useNewUrlParser: true });

        
        // START SERVER //
        await server.start();
        console.log('Server running at:', server.info.uri);
        

    } catch (err) {
        console.log(err);
        process.exit(1);
    }
};

start();