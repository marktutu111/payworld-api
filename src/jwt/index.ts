import * as jsonwebtoken from "jsonwebtoken";
import { secrete } from "./secrete";

// JSON WEBTOKEN EXPIRY EXAMPLE
// 86400 -> 24hours
const jwt = {
    pkg: {
        name: 'jwt',
        version: '1'
    },
    register: async (server) => {
        server.method('generateJWT', (data,expiry?: number | string) => {
            return jsonwebtoken.sign(
                data,
                secrete, 
                {
                    algorithm: 'HS256', 
                    expiresIn: expiry
                }
            )
        });
    }
}


export default jwt;