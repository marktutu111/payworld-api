import { formatePhonenumber } from "../modules/formate-phone-number";
import * as moment from "moment";
import * as mongoose from "mongoose";



const LoanReminder=async(server)=>{
    try {
        const loans=await server.methods.dbLoans.find(
            {
                status:'pending',
                paidout:true,
                defaulted:false,
                remind_date: {
                    $lte:new Date().toISOString()
                }
            }
        ).limit(1000).sort({createdAt:-1}).populate('customer');
        loans.forEach(async loan=>{
            try {
                const {mobile,firstname}=loan['customer'];
                const message = 'Hello ' + firstname+'!'
                + '\n'
                + 'You are reminded to pay your loan before '
                + new Date(loan.next_payment_date).toDateString()
                + ' to avoid penalties. You owe an amount GHS'
                + loan.amount_remaining + '.'
                + '\n'
                + 'Thank you for using PayGenie'
                server.methods.nsanoSendSMS(
                    {
                        type:'single',
                        data:{
                            recipient:formatePhonenumber(mobile),
                            message:message
                        }
                    }
                ).catch(err=>server.methods.dbErrors.create(
                    {
                        id:'SMS | ERROR',
                        message:err.message,
                        error:err
                    }
                )); 
            } catch (err) {
                server.methods.dbErrors.create(
                    {
                        id:'reminders-error',
                        message:err.message,
                        error:err
                    }
                ).catch(err=>null);
            }
        })
    } catch (err) {
        server.methods.dbErrors.create(
            {
                id:'reminders-error',
                message:err.message,
                error:err
            }
        ).catch(err=>null);
    }
}



const task=async(server)=>{
    try {

        const loans=await server.methods.dbLoans.find(
            {
                status:'pending',
                paidout:true,
                defaulted:false,
                canBorrow:true,
                next_payment_date: {
                    $lte: new Date().toISOString()
                }
            }
        ).limit(1000).sort({createdAt:-1});
        loans.forEach(async loan=>{
            try {
                const {amount_remaining,penalty,customer}=loan;
                const penalty_amount:number = (penalty / 100) * amount_remaining;
                const newAmount:number= amount_remaining+penalty_amount;
                loan.update(
                    {
                        defaulted:true,
                        canBorrow:false,
                        updateAt:Date.now(),
                        $inc:{
                            amount_remaining:penalty_amount,
                            penalty_amount:penalty_amount
                        }
                    }
                ).catch(err=>null);
                const customerDetails = await server.methods.dbCustomers.findOneAndUpdate(
                    {
                        _id:customer
                    },
                    {
                        $set:{
                            current_loan:newAmount,
                            updatedAt:Date.now()
                        }
                    }
                )
                const {mobile,firstname}=customerDetails;
                const message = 'Hello ' + firstname 
                + '\n'
                + 'Your loan was not fully repaid,'
                + 'a ' + penalty + '% late payment fee has been added. '
                + 'You now owe GHS ' + parseFloat(newAmount.toString()).toFixed(2) + ' .'
                + 'Please pay your loan as soon as you can.'
                + '\n'
                + 'Thank you for using PayGenie'
                server.methods.nsanoSendSMS(
                    {
                        type:'single',
                        data:{
                            recipient:formatePhonenumber(mobile),
                            message:message
                        }
                    }
                ).catch(err=>server.methods.dbErrors.create(
                    {
                        id:'SMS | ERROR',
                        message:err.message,
                        error:err
                    }
                )); 
            } catch (err) {
                server.methods.dbErrors.create(
                    {
                        id:loan._id,
                        message:err.message,
                        error:err
                    }
                ).catch(err=>null);
            }
        })
    } catch (err) {
        server.methods.dbErrors.create(
            {
                id:'cron',
                message:err.message,
                error:err
            }
        ).catch(err=>null);
    }
}



const generateInvoice=async(server)=>{
    try {
    
        const employers=await server.methods.dbEmployer.find(
            {
                salary_payment_date:new Date().getDate().toString()
            }
        );
        employers.forEach(async employer=>{
            try {
                const {
                    _id,
                    email,
                    name,
                    salary_payment_day,
                    penalty_accrued
                }=employer;
                const results= await server.methods.dbLoans.aggregate(
                    [
                        { $match: {
                                employer:mongoose.Types.ObjectId(_id),
                                status:'pending'
                            } 
                        },
                        {
                            $group: {
                                _id: null,
                                amountBorrowedWithInterest: { $sum: "$amount_remaining" },
                                amountBorrowed: { $sum: "$amount" },
                                count: { $sum: 1 },
                                loans:{
                                    $push: "$$ROOT"
                                }
                            }
                        }
                    ]
                );

                if(!results[0]){
                    return;
                }

                const loans=results[0];
                const invoces_count=await server.methods.dbInvoice.count({});

                const invdata={
                    invoiceId:`INV-${invoces_count+1}`,
                    amount:loans['amountBorrowedWithInterest']+penalty_accrued,
                    employer:_id,
                    loans:loans['loans'],
                    totalLoans:loans['count'],
                    amountBorrowed:loans['amountBorrowed'],
                    amountBorrowedWithInterest:loans['amountBorrowedWithInterest'],
                    penalty_accrued:penalty_accrued
                }
    
                const invoice=await server.methods.dbInvoice.create(invdata);
                if (!invoice || !invoice._id){
                    throw Error('Error saving invoice');
                }
                          
                const html=await server.methods.generateInvoice(
                    {
                        employer:name,
                        date:Date.now(),
                        invoiceNumber:invoice.invoiceId,
                        amountBorrowed:loans.amountBorrowed,
                        totalLoans:loans.count,
                        amountBorrowedWithInterest:parseFloat(invdata.amount).toFixed(2),
                        due:moment().add('days',salary_payment_day).format('l').toString(),
                        penalty_accrued:invdata.penalty_accrued
                    }
                );

                const pdf= await server.methods.createPDFStream(html);
                return server.methods.SendEmail(
                    {
                        from:'info@mypaygenie.com',
                        to: email,
                        subject:'PayGenie Invoice',
                        html:`
                            <p>Hello ${name}</p>
                            <p>Kindly find attached your invoice for this month.</p>
                            <p>Best Regards.</p>
                        `,
                        attachments:[
                            {
                                filename: 'invoice.pdf',
                                content:pdf
                            }
                        ]
                    }
                ).catch(err=>server.methods.dbErrors.create(
                    {
                        id:_id,
                        message:err.message,
                        error:err
                    }
                ));
            } catch (err) { 
                server.methods.dbErrors.create(
                    {
                        id:employer._id,
                        message:err.message,
                        error:err
                    }
                )
            }
        })
    } catch (err) {
        server.methods.dbErrors.create(
            {
                id:'CRON ERROR',
                message:err.message,
                error:err
            }
        )
    }
}

export default {
    task,
    generateInvoice,
    LoanReminder
};