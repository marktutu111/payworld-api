const CronJob = require('cron').CronJob;
import Handler from "./cron.task";

const cron ={
    pkg:{
        name:'cron',
        version:'1'
    },
    register:async(server)=>{
        const task = new CronJob('36 5 * * *',function(){
            Handler.task(server);
            Handler.LoanReminder(server);
        });

        const invoice = new CronJob('40 5 * * *',function(){
            Handler.generateInvoice(server)
        });

        task.start();
        invoice.start();

    }
}

export default cron; 